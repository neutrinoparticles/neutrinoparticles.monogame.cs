# NeutrinoParticles.MonoGame

The package allows you to render [NeutrinoParticles](https://neutrinoparticles.com/) effects in [MonoGame](http://monogame.net/) game framework.

## API documentation

For more info about latest API you can check [API documentation](https://neutrinoparticles.gitlab.io/neutrinoparticles.monogame.cs-doc/master/) pages.

For specific library version refer to [Releases](https://gitlab.com/neutrinoparticles/neutrinoparticles.monogame.cs/-/releases) page.

## Issues and Requests

If you have any request or issue, please, feel free to post it on [Issues](https://gitlab.com/neutrinoparticles/neutrinoparticles.monogame.cs/issues) page. It will be considered ASAP.

## Installation

There are two libraries which you will need to install for your MonoGame project to use NeutrinoParticles.
1. The first one is `NeutrinoParticles.Core` library. It can be installed **only** as NuGet package. And it will allow you to attach exported effects as usual classes to you project.
2. The second one is `NeutrinoParticles.MonoGame` represented by this repository. It can be installed in a few different ways described below.

### Install from NuGet

You can use [NuGet](https://nuget.org/) to install both packages to your project.

#### NuGet from Command Line

To make it on Windows from command line or on MacOS/Linux, please refer to:
1. [NuGet CLI installation.](https://docs.microsoft.com/en-us/nuget/reference/nuget-exe-cli-reference)
2. [nuget install](https://docs.microsoft.com/en-us/nuget/reference/cli-reference/cli-ref-install) command reference.

#### NuGet from Visual Studio

To install packages in Visual Studio, please refer to [this instructions](https://docs.microsoft.com/en-us/nuget/quickstart/install-and-use-a-package-in-visual-studio).

### Install as DLL

Alternatively, you can download `NeutrinoParticles.MonoGame.dll` from [Releases](https://gitlab.com/neutrinoparticles/neutrinoparticles.monogame.cs/-/releases) page
and attach it to your project.

### Install as source codes

Or you can download source code and add/import `NeutrinoParticles.MonoGame.csproj` project to your solution.

## Quick usage

> This quick usage guide shows only programming side of using `NeutrinoPartciles.MonoGame` library. 
It assumes that you know how to use NeutrinoParticles Editor and already created effect and exported it to your project.
>
> Also, it assumes you use default MonoGame project template.

In your ancestor of `Game` class add fields to store main NeutrinoParticles context and effect to draw.

```csharp
public class Game1 : Game
{
	...
	private NeutrinoParticles.MonoGame.Context neutrinoContext_;
	private NeutrinoParticles.MonoGame.ParticleEffect effect_;
```

Create the context in `LoadContent()`.

```csharp
	protected override void LoadContent()
	{
		neutrinoContext_ = new NeutrinoParticles.MonoGame.Context(
			GraphicsDevice,
			texturesBasePath: "particles/", // Prefix directory for textures.
			generateNoise: true             // Allows to use turbulence.
		);
```

Later in the same function create effect model.

```csharp
		var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(
			neutrinoContext_,
			new NeutrinoParticles.Effect_MyCoolEffect(), // Instance of exported effect model.
			Content
		);
```

And create instance of effect to render on the screen.

```csharp
		effect_ = new NeutrinoParticles.MonoGame.ParticleEffect(
			effectModel,
			position: new Vector3(400, 300, 0)
		);
	}
```

Update effect.

```csharp
	protected override void Update(GameTime gameTime)
	{
		...
		effect_.Update(gameTime);
		...
	}
```

Draw effect.

```csharp
	protected override void Draw(GameTime gameTime)
	{
		...
		effect_.Draw();
		...
	}
```

## CI configuring

### Install VS2022
* Install it with .NET development for desktops
* Add `MSBuild` to `PATH`: `C:\Program Files\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\`
* Add `TestPlatform` to `PATH`: `C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\Extensions\TestPlatform\`

### nuget
* Download it from https://www.nuget.org/downloads and add to `PATH`

### .NET Core 3.1
* Download and install it from https://dotnet.microsoft.com/en-us/download/dotnet/3.1
* Restart to PATH take the effect

### Doxygen
* https://www.doxygen.nl/download.html
* Restart to PATH take the effect

### For VirtualBox

Machine must have GPU acceleration ON.

GitLab runner should run in manual, but not service mode. Otherwise there will be DirectX issues for Tests.

## Contribution

We will be much appreciated for any fix/feature merge requests and will consider it ASAP.

There are not much rules for such requests, just be sure that all tests are passing.