﻿
using Microsoft.Xna.Framework;

namespace NeutrinoParticles.MonoGame
{
    /// <summary>
    /// Describes placement of texture on atlas.
    /// </summary>
    public class TexCoordRemap
    {
        /// <summary>
        /// Offset of texture quad on atlas. In range [0;1].
        /// </summary>
        public Vector2 offset;
        /// <summary>
        /// Size of texture quad on atlas. In range [0;1].
        /// </summary>
        public Vector2 size;

        /// <summary>
        /// Creates texture remapping object. 
        /// </summary>
        /// <param name="offset">Offset of texture quad on atlas. In range [0;1].</param>
        /// <param name="size">Size of texture quad on atlas. In range [0;1].</param>
        public TexCoordRemap(Vector2 offset, Vector2 size)
        {
            this.offset = offset;
            this.size = size;
        }
    }
}
