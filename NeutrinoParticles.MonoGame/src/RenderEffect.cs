﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace NeutrinoParticles.MonoGame
{
    internal class RenderEffect : Microsoft.Xna.Framework.Graphics.Effect
    {
        private EffectParameter textureParam;
        private EffectParameter worldViewProjParam;

        private EffectTechnique defaultTechnique;
        private EffectTechnique multiplyTechnique;

        public Matrix WorldViewProj
        {
            get { return worldViewProjParam.GetValueMatrix(); }
            set { worldViewProjParam.SetValue(value); }
        }
        
        public Texture2D Texture
        {
            get { return textureParam.GetValueTexture2D(); }
            set { textureParam.SetValue(value); }
        }

        public RenderEffect(GraphicsDevice device, byte[] bytecode)
            : base(device, bytecode)
        {
            worldViewProjParam = Parameters["WorldViewProj"];
            textureParam = Parameters[Utils.DeviceIsDirectX() ? "Texture" : "TextureSampler+Texture"];

            defaultTechnique = Techniques["Default"];
            multiplyTechnique = Techniques["Multiply"];

            SetDefaultTechnique();
        }

        public void SetDefaultTechnique()
        {
            CurrentTechnique = defaultTechnique;
            CurrentTechnique.Passes[0].Apply();
        }

        public void SetMultiplyTechnique()
        {
            CurrentTechnique = multiplyTechnique;
            CurrentTechnique.Passes[0].Apply();
        }
    }
}
