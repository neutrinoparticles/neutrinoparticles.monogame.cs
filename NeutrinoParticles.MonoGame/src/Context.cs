﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;

namespace NeutrinoParticles.MonoGame
{
    /// <summary>
    /// Main context for all effects in the application.
    /// <para>
    /// This is a shared object for effects in the application. Create it before
    /// any effect created and keep it alive till the end of the application.
    /// </para>
    /// 
    /// <para>
    /// You will need <c>GraphicsDevice</c> to create the context, so the best
    /// place to do this is in the start of <c>LoadContent()</c> of your 
    /// <c>Microsoft.Xna.Framework.Game</c> ancestor.
    /// </para>
    /// 
    /// <para>
    /// To be able to use noise (turbulence) in your effects, you will need to
    /// set <c>generateNoise</c> flag in the constructor or to call 
    /// <see cref="GenerateNoise()"/> afterwards.
    /// </para>
    /// 
    /// <example>
    /// <code>
    /// class MyGame : public Microsoft.Xna.Framework.Game
    /// {
    ///     private NeutrinoParticles.MonoGame.Context neutrinoContext_;
    /// 
    ///     ...
    /// 
    ///     protected override void LoadContent()
    ///     {
    ///         neutrinoContext_ = new NeutrinoParticles.MonoGame.Context(GraphicsDevice,
    ///             textureBasePath: "particles",
    ///             generateNoise: true);
    ///             
    ///         // Load textures and create effects here
    ///     }
    ///     
    ///     protected override void UnloadContent()
    ///     {
    ///         if (neutrinoContext_ != null)
    ///             neutrinoContext_.Shutdown();
    ///     }
    ///     
    ///     ...
    /// }
    /// </code>
    /// </example>
    /// </summary>
    public class Context
    {
        private GraphicsDevice graphicsDevice_;
        private Renderer renderer_;

        /// <summary>
        /// Creates the context.
        /// </summary>
        /// <param name="graphicsDevice">Graphics device to render.</param>
        /// <param name="texturesBasePath">Prefix will be added to textures specified in exported effects.</param>
        /// <param name="generateNoise">Will call <see cref="GenerateNoise()"/> after construction.</param>
        /// <param name="axisYGoesUp">Specifies if Y axis of coordinate system of your application goes up. Otherwise is goes down.</param>
        public Context(GraphicsDevice graphicsDevice, 
            String texturesBasePath = "", 
            bool generateNoise = false,
            bool axisYGoesUp = false)
        {
            graphicsDevice_ = graphicsDevice;
            TexturesBasePath = texturesBasePath;
            AxisYGoesUp = axisYGoesUp;

            var batcher = new Batcher(graphicsDevice);

            RenderEffect effect;
            {
                var assembly = typeof(Context).Assembly;
                var isDirectX = Utils.DeviceIsDirectX();
                var stream = assembly.GetManifestResourceStream(isDirectX ?
                    "NeutrinoParticles.MonoGame.DX.Resources.Effect.mgfxo" :
                    "NeutrinoParticles.MonoGame.GL.Resources.Effect.mgfxo");
                var ms = new MemoryStream();
                stream.CopyTo(ms);

                effect = new RenderEffect(graphicsDevice, ms.ToArray());
            }

            BlendState multiplyBlendState;
            {
                var bs = new BlendState();

                bs.AlphaBlendFunction = BlendFunction.Add;
                bs.ColorBlendFunction = BlendFunction.Add;

                bs.ColorSourceBlend = Blend.Zero;
                bs.ColorDestinationBlend = Blend.SourceColor;

                bs.AlphaSourceBlend = Blend.Zero;
                bs.AlphaDestinationBlend = Blend.One;

                multiplyBlendState = bs;
            }

            BlendState addBlendState;
            {
                var bs = new BlendState();

                bs.AlphaBlendFunction = BlendFunction.Add;
                bs.ColorBlendFunction = BlendFunction.Add;

                bs.ColorSourceBlend = Blend.One;
                bs.ColorDestinationBlend = Blend.One;

                bs.AlphaSourceBlend = Blend.Zero;
                bs.AlphaDestinationBlend = Blend.One;

                addBlendState = bs;
            }

            renderer_ = new Renderer(graphicsDevice, effect, addBlendState, multiplyBlendState, batcher);

            if (generateNoise)
                GenerateNoise();
        }

        /// <summary>
        /// Shutsdown the context. After this call the context and all effects of this context
        /// become unusable. Has to be called in the end of the application.
        /// </summary>
        public void Shutdown()
        {
            renderer_.Shutdown();
        }

        /// <value>Graphics device to render.</value>
        public GraphicsDevice GraphicsDevice { get { return graphicsDevice_; } }

        /// <value>Prefix for textures described in effects.</value>
        public String TexturesBasePath { get; }

        /// <value>Direction of Y axis. Up if true, otherwise - down.</value>
        public bool AxisYGoesUp { get; }

        internal Renderer Renderer { get { return renderer_; } }

        /// <summary>
        /// Starts consequent rendering of effects.
        /// <para>
        /// Use it for more optimized rendering when you render several effects in row.
        /// It will prevent exessive rendering states change.
        /// </para>
        /// <para>
        /// You don't need to use it to render only one effect.
        /// </para>
        /// <para>
        /// <see cref="EndDraw()"/> must be called afterwards.
        /// </para>
        /// <example>
        /// <code>
        /// neutrinoContext_.BeginRender();
        /// effect1.Draw();
        /// effect2.Draw();
        /// effect3.Draw();
        /// neutrinoContext_.EndRender();
        /// 
        /// // other rendering code
        /// 
        /// neutrinoContext_.BeginRender();
        /// effect4.Draw();
        /// effect5.Draw();
        /// neutrinoContext_.EndRender();
        /// </code>
        /// </example>
        /// </summary>
        public void BeginDraw()
        {
            renderer_.BeginDraw();
        }

        /// <summary>
        /// Ends consequent rendering of effects.
        /// <para>
        /// See <see cref="BeginDraw()"/> for details.
        /// </para>
        /// </summary>
        public void EndDraw()
        {
            renderer_.EndDraw();
        }

        /// <summary>
        /// Generates noise (turbulence) texture.
        /// <para>
        /// You can use <c>generateNoise</c> param of <see cref="Context"/> constructor.
        /// to call this method right after construction.
        /// </para>
        /// </summary>
        public void GenerateNoise()
        {
            var noiseGenerator = NeutrinoParticles.Context.Instance.startNoiseGeneration();

            if (noiseGenerator != null)
            {
                while (!noiseGenerator.step()) { }
            }
        }

        /// <summary>
        /// Creates default projection matrix used for rendering effects.
        /// <para>
        /// It will be Ortho projection matrix with (0; 0) in the left top corner
        /// of the screen and (screenWidth;screenHeight) in the bottom right corner.
        /// </para>
        /// </summary>
        /// <returns>Projection matrix.</returns>
        public Matrix CreateDefaultProjectionMatrix()
        {
            var width = GraphicsDevice.PresentationParameters.BackBufferWidth;
            var height = GraphicsDevice.PresentationParameters.BackBufferHeight;
            return Matrix.CreateOrthographicOffCenter(0, width, height, 0, -10000, 10000);
        }

        /// <summary>
        /// Creates default view matrix which is basically identity matrix.
        /// </summary>
        /// <returns>View matrix</returns>
        public Matrix CreateDefaultViewMatrix()
        {
            return Matrix.Identity;
        }
    }
}
