﻿using System;
using System.Reflection;
using Microsoft.Xna.Framework;

namespace NeutrinoParticles.MonoGame
{
    internal class Utils
    {
        static public bool DeviceIsDirectX()
        {
            // This is a hacky check of the graphics engine.
            var mgAssembly = Assembly.GetAssembly(typeof(Game));
            var shaderType = mgAssembly.GetType("Microsoft.Xna.Framework.Graphics.Shader");
            var profileProperty = shaderType.GetProperty("Profile");
            var value = (int)profileProperty.GetValue(null);
            return value == 1;
        }
    }
}
