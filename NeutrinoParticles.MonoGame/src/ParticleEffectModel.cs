﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace NeutrinoParticles.MonoGame
{
    /// <summary>
    /// Provides information about textures placed on atlases.
    /// 
    /// <para>
    /// This delegate intended to provide <see cref="ParticleEffectModel"/> with
    /// information about textures placed on atlases.
    /// </para>
    /// 
    /// <para>
    /// Effect model will use this delegate to lookup textures on atlases. Since
    /// MonoGame doesn't support atlases internally, it is up to you how to implement
    /// this delegate. Probably, you will need to write some conversion code
    /// to support atlases extension you use.
    /// </para>
    /// 
    /// <para>
    /// On construction, <see cref="ParticleEffectModel"/> will use provided delegate
    /// to check every single texture described in the effect it represents.
    /// Each texture will be passed to the delegate two times: with and without extension
    /// in its path. The first positive result will be taken.
    /// </para>
    /// 
    /// <para>
    /// In case texture is not found on atlases and delegate returned <c>null</c>, the 
    /// texture will be loaded by <see cref="ParticleEffectModel"/> as standalone one.
    /// </para>
    /// 
    /// </summary>
    /// 
    /// <param name="textureName">Texture path specified in effect to lookup.</param>
    /// 
    /// <returns><see cref="AtlasTexture"/> description of the texture or <c>null</c>
    /// if the texture is not placed on any atlas.</returns>
    public delegate AtlasTexture AtlasesLookupDelegate(string textureName);

    /// <summary>
    /// Effect model suitable to use in MonoGame environment.
    /// 
    /// <para>
    /// For each type of effect you create only one effect model,
    /// and as many instances of the effect as you want to. The model is constant
    /// object and contains behavior of the effect. An instance is actual effect 
    /// on the screen and contains state of the effect (position, 
    /// rotation and particles), and can be simulated by time.
    /// </para>
    /// 
    /// <para>
    /// This class is simply a wrap around NeutrinoParticles effect model. Additionally,
    /// it stores information about texture objects used to render instances of
    /// this effect, and coordinate remappings if some of textures are placed on
    /// atlases.
    /// </para>
    /// 
    /// <remarks>It is important to have all necessary texture atlases loaded 
    /// before creating effect model.</remarks>
    /// 
    /// <example>
    /// <code>
    /// protected override void LoadContent()
    /// {
    ///     ...
    ///     
    ///     // Create exported NeutrinoParticles model which is usual class and has to be 
    ///     // attached to your project and be visible from the code.
    ///     var neutrinoEffectModel = new NeutrinoParticles.Effect_MyCoolEffect();
    ///     
    ///     // Create MonoGame particle effect model.
    ///     var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(
    ///         neutrinoContext, neutrinoEffectModel, Content);
    ///         
    ///     ...
    /// }
    /// </code>
    /// </example>
    /// </summary>
    public class ParticleEffectModel
    {
        private Context context_;

        private NeutrinoParticles.EffectModel neutrinoModel_;
        private Texture2D[] textures_;
        private TexCoordRemap[] texCoordRemap_;

        /// <summary>
        /// Creates <see cref="ParticleEffectModel"/>.
        /// </summary>
        /// <param name="context"><see cref="Context"/> used for effects in the application.</param>
        /// <param name="neutrinoEffectModel">NeutrinoParticles model of the effect.</param>
        /// <param name="content">Content manager used in the game to load textures.</param>
        /// <param name="texturesBasePath">Override of Context.TexturesBasePath property. Adds prefix path to the textures loaded by this effect model. If null, Context.TexturesBasePath used.</param>
        /// <param name="atlasesLookup">Atlases lookup delegate. See <see cref="AtlasesLookupDelegate"/>. </param>
        public ParticleEffectModel(Context context, NeutrinoParticles.EffectModel neutrinoEffectModel, 
            ContentManager content, string texturesBasePath = null, AtlasesLookupDelegate atlasesLookup = null)
        {
            context_ = context;
            neutrinoModel_ = neutrinoEffectModel;

            uint numTextures = (uint)neutrinoEffectModel.textures().Length;
            textures_ = new Texture2D[numTextures];
            texCoordRemap_ = new TexCoordRemap[numTextures];

            for (uint texIndex = 0; texIndex < numTextures; ++texIndex)
            {
                var texturePath = neutrinoEffectModel.textures()[texIndex];
                string texturePathNoExt = texturePath;
                {
                    int extPos = texturePath.LastIndexOf('.');
                    if (extPos >= 0)
                        texturePathNoExt = texturePath.Substring(0, extPos);
                }

                AtlasTexture atlasTexture = null;

                if (atlasesLookup != null)
                {
                    atlasTexture = atlasesLookup(texturePath);
                    if (atlasTexture == null)
                        atlasTexture = atlasesLookup(texturePathNoExt);
                }

                if (atlasTexture != null)
                {
                    textures_[texIndex] = atlasTexture.texture;
                    texCoordRemap_[texIndex] = atlasTexture.texCoordRemap;
                }
                else
                {
                    string textureFullPath = (texturesBasePath == null ? context_.TexturesBasePath : texturesBasePath) +
                        texturePathNoExt;

                    textures_[texIndex] = content.Load<Texture2D>(textureFullPath);

                    texCoordRemap_[texIndex] = null;
                }
            }
        }

        /// <value><see cref="Context"/> used for effects in the application.</value>
        public Context Context { get { return context_; } }

        /// <value>NeutrinoParticles effect model.</value>
        public NeutrinoParticles.EffectModel NeutrinoModel { get { return neutrinoModel_; } }

        /// <value>Array of textures used in the effect.</value>
        public Texture2D[] Textures { get { return textures_; } }

        /// <value>Array of texture coordinates remappings. 
        /// Indices correspond to <see cref="Textures"/> array. 
        /// Elements can be <c>null</c> if corresponding texture is not on atlas.</value>
        public TexCoordRemap[] TexCoordRemap {  get { return texCoordRemap_; } }
    }
}
