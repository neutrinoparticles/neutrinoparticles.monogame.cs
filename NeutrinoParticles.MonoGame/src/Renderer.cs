﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace NeutrinoParticles.MonoGame
{
    internal class Renderer
    {
        private GraphicsDevice device_;
        private RenderEffect effect_;
        private BlendState addBlendState_;
        private BlendState multiplyBlendState_;
        private Batcher batcher_;

        private bool inRender_ = false;
        private NeutrinoParticles.RenderMaterial currentMaterial_ = NeutrinoParticles.RenderMaterial.Normal;
        private Texture2D currentTexture_ = null;

        public Renderer(GraphicsDevice device, RenderEffect effect, BlendState addBlendState,
            BlendState multiplyBlendState, Batcher batcher)
        {
            device_ = device;
            effect_ = effect;
            addBlendState_ = addBlendState;
            multiplyBlendState_ = multiplyBlendState;
            batcher_ = batcher;
        }

        public void Shutdown()
        {
            effect_.Dispose();
        }

        public Matrix WorldViewProj { set { effect_.WorldViewProj = value; } }

        public bool InRender { get { return inRender_; } }

        public void BeginDraw()
        {
            if (inRender_)
                return;

            inRender_ = true;

            device_.DepthStencilState = DepthStencilState.None;
            device_.RasterizerState = RasterizerState.CullNone;
            device_.SamplerStates[0] = SamplerState.LinearClamp;

            currentTexture_ = null;
        }

        public void EndDraw()
        {
            if (!inRender_)
                return;

            Flush();
            inRender_ = false;
        }

        public void BeginMaterial(RenderMaterial material, Texture2D texture)
        {
            bool shouldFlush = false;

            if (currentTexture_ != texture)
            {
                shouldFlush = true;
            }

            if (currentMaterial_ != material)
            {
                shouldFlush = true;
            }

            if (shouldFlush)
            {
                Flush();

                effect_.Texture = texture;

                switch (material)
                {
                    case RenderMaterial.Normal:
                        effect_.SetDefaultTechnique();
                        device_.BlendState = BlendState.AlphaBlend;
                        break;

                    case RenderMaterial.Add:
                        effect_.SetDefaultTechnique();
                        device_.BlendState = addBlendState_;
                        break;

                    case RenderMaterial.Multiply:
                        effect_.SetMultiplyTechnique();
                        device_.BlendState = multiplyBlendState_;
                        break;
                }

                currentTexture_ = texture;
                currentMaterial_ = material;
            }
        }

        public void BatchQuad(VertexPositionColorTexture[] vertices)
        {
            batcher_.BatchQuad(vertices);
        }

        private void Flush()
        {
            batcher_.Flush();
        }
    }
}
