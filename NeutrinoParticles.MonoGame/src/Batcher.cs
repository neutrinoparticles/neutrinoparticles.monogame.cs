﻿using Microsoft.Xna.Framework.Graphics;
using System;

namespace NeutrinoParticles.MonoGame
{
    internal class Batcher
    {
        private const int InitialQuadsSize = 500;
        private const int MaxQuads = short.MaxValue / 4;

        private readonly GraphicsDevice device_;

        private VertexPositionColorTexture[] vertices_;
        private short[] indices_;

        private int quadsCount_ = 0;

        public Batcher(GraphicsDevice device)
        {
            device_ = device;
            EnsureArrayCapacity(InitialQuadsSize);
        }

        private unsafe void EnsureArrayCapacity(int numQuads)
        {
            int neededCapacity = 6 * numQuads;
            if (indices_ != null && neededCapacity <= indices_.Length)
            {
                return;
            }
            short[] newIndex = new short[6 * numQuads];
            int start = 0;
            if (indices_ != null)
            {
                indices_.CopyTo(newIndex, 0);
                start = indices_.Length / 6;
            }
            fixed (short* indexFixedPtr = newIndex)
            {
                var indexPtr = indexFixedPtr + (start * 6);
                for (var i = start; i < numQuads; i++, indexPtr += 6)
                {
                    *(indexPtr + 0) = (short)(i * 4 + 0);
                    *(indexPtr + 1) = (short)(i * 4 + 1);
                    *(indexPtr + 2) = (short)(i * 4 + 2);

                    *(indexPtr + 3) = (short)(i * 4 + 2);
                    *(indexPtr + 4) = (short)(i * 4 + 3);
                    *(indexPtr + 5) = (short)(i * 4 + 0);
                }
            }
            indices_ = newIndex;

            vertices_ = new VertexPositionColorTexture[4 * numQuads];
        }

        public unsafe void BatchQuad(VertexPositionColorTexture[] vertices)
        {
            if (vertices_.Length < (quadsCount_ + 1) * 4)
            {
                Flush();
                EnsureArrayCapacity((int)(Math.Min(quadsCount_ * 1.5, MaxQuads)));
            }
            
            fixed (VertexPositionColorTexture* vertexArrayPtr = vertices_)
            {
                var ptr = vertexArrayPtr + quadsCount_ * 4;

                *(ptr + 0) = vertices[0];
                *(ptr + 1) = vertices[1];
                *(ptr + 2) = vertices[2];
                *(ptr + 3) = vertices[3];
            }

            ++quadsCount_;
        }

        public void Flush()
        {
            if (quadsCount_ < 1)
                return;

            int vertexCount = quadsCount_ * 4;

            device_.DrawUserIndexedPrimitives(
                PrimitiveType.TriangleList,
                vertices_,
                0,
                vertexCount,
                indices_,
                0,
                quadsCount_ * 2,
                VertexPositionColorTexture.VertexDeclaration);

            quadsCount_ = 0;
        }
    }
}
