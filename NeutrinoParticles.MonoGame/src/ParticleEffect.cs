﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace NeutrinoParticles.MonoGame
{
    /// <summary>
    /// NeutrinoParticles effect instance to render in MonoGame environment.
    /// 
    /// <para>
    /// This class represents instance of <see cref="ParticleEffectModel"/>
    /// on the screen. It is a state of the effect, which has position,
    /// rotation and can be rendered and simulated by time.
    /// </para>
    /// 
    /// <para>
    /// <b>Don't change scale of the effect when there are still some particles
    /// on the screen!</b> It will lead to undesired particles shifting.
    /// </para>
    /// 
    /// <para>
    /// If you don't know position of the effect at the creation moment, just
    /// create it paused. Afterwards you can set position by <see cref="Position"/> 
    /// and unpause by <see cref="Paused"/>. In this case you will not have any
    /// undesired trails or particles shot in wrong place.
    /// </para>
    /// 
    /// <example><code>
    /// class MyGame : public Microsoft.Xna.Framework.Game
    /// {
    ///     private List &lt; NeutrinoParticles.MonoGame.ParticleEffect &gt; effects_;
    ///     
    ///     protected override void LoadContent()
    ///     {
    ///         ...
    ///     
    ///         // Create effect model.
    ///         var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(
    ///             neutrinoContext, new NeutrinoParticles.Effect_MyCoolEffect(), Content);
    ///         
    ///         // Create effects.
    ///         effects_.Add(new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
    ///             position: new Vector3(100, 100, 0)));
    ///         effects_.Add(new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
    ///             position: new Vector3(200, 200, 0),
    ///             rotation: Quaternion.CreateFromAxisAngle(new Vector3(0, 0, 1), MathHelper.ToRadians(45))));
    ///             
    ///         ...
    ///     }
    ///     
    ///     protected override void Update(GameTime gameTime)
    ///     {
    ///         ...
    ///         
    ///         // Update effects
    ///         foreach (var effect in effects_)
    ///             effect.Update((float)gameTime.ElapsedGameTime.Milliseconds / 1000.0f);
    ///             
    ///         ...
    ///     }
    /// 
    ///     protected override void Draw(GameTime gameTime)
    ///     {
    ///         ...
    ///         
    ///         // Render effects
    ///         foreach (var effect in effects_)
    ///             effect.Draw();
    ///             
    ///         ...
    ///     }
    /// 
    /// </code></example>
    /// </summary>
    public class ParticleEffect : NeutrinoParticles.RenderBuffer
    {
        private Renderer renderer_;
        private ParticleEffectModel model_;
        private NeutrinoParticles.Effect neutrinoEffect_;

        private VertexPositionColorTexture[] temp_;
        private TexCoordRemap texCoordRemap_;

        /// <summary>
        /// Creates instance of effect.
        /// </summary>
        /// <param name="model"><see cref="ParticleEffectModel"/> to create instance of.</param>
        /// <param name="position">Position of the effect. By default will be {0; 0; 0}.</param>
        /// <param name="rotation">Rotation of the effect. By default will be identity quaternion.</param>
        /// <param name="scale">Scale of the effect. Value <c>2</c> means twice bigger.</param>
        /// <param name="paused">Whether to create paused effect.</param>
        /// <param name="generatorsPaused">Whether to pause generators of created effect.</param>
        public ParticleEffect(ParticleEffectModel model, 
            Vector3? position = null, 
            Quaternion? rotation = null,
            Vector3? scale = null,
            bool paused = false,
            bool generatorsPaused = false)
        {
            Context = model.Context;
            renderer_ = Context.Renderer;
            model_ = model;

            if (position != null)
                Position = position.Value;
            else
                Position = Vector3.Zero;

            if (rotation != null)
                Rotation = rotation.Value;
            else
                Rotation = Quaternion.Identity;

            if (scale != null)
                Scale = scale.Value;
            else
                Scale = Vector3.One;

            neutrinoEffect_ = new NeutrinoParticles.Effect(model_.NeutrinoModel, this, 
                position: NeutrinoVec3(Position), 
                rotation: NeutrinoQuat(Rotation),
                paused: paused,
                generatorsPaused: generatorsPaused);

            temp_ = new VertexPositionColorTexture[4];
        }

        /// <value>Underlying NeutrinoParticles effect.</value>
        public NeutrinoParticles.Effect NeutrinoEffect { get { return neutrinoEffect_; } }
        
        /// <value><see cref="Context"/> used for creating this effect.</value>
        public Context Context { get; private set; }

        /// <value>Position of the effect.</value>
        public Vector3 Position { get; set; }

        /// <value>Rotation of the effect.</value>
        public Quaternion Rotation { get; set; }

        /// <value>Scle of the effect. <b>Don't change it when there are particles on the screen!</b></value>
        public Vector3 Scale { get; set; }

        /// <value>Specifies if effect is paused. In this case all particles are frozen.</value>
        public bool Paused {
            get { return neutrinoEffect_.Paused; }
            set
            {
                if (!value && neutrinoEffect_.Paused)
                    InstantRelocate(Position, Rotation);

                neutrinoEffect_.Paused = value;
            } }

        /// <value>Specifies if generators in the effect are paused. In this case effect is still simulated, but no new particles emitted.</value>
        public bool GeneratorsPaused
        {
            get { return neutrinoEffect_.GeneratorsPaused; }
            set { neutrinoEffect_.GeneratorsPaused = value; }
        }

        /// <summary>
        /// Completely resets the effect.
        /// 
        /// <para>
        /// All particles will be deleted. The same as create new ParticleEffect.
        /// </para>
        /// </summary>
        /// <param name="position">New position if defined. Otherwise don't change position.</param>
        /// <param name="rotation">New rotation if defined. Otherwise don't change rotation.</param>
        /// <param name="paused">Whether to pause the effect after reset.</param>
        /// <param name="generatorsPaused">Whether to pause generators after reset.</param>
        public void Reset(Vector3? position = null, Quaternion? rotation = null, bool paused = false, bool generatorsPaused = false)
        {
            neutrinoEffect_.reset(
                position.HasValue ? NeutrinoVec3(position.Value) : (NeutrinoParticles._math.vec3?)null,
                rotation.HasValue ? NeutrinoQuat(rotation.Value) : (NeutrinoParticles._math.quat?)null,
                paused, generatorsPaused);
        }

        /// <summary>
        /// Instantly changes position|rotation of the effect.
        /// 
        /// <para>
        /// Such instant changing will prevent particles trail from previous effect location.
        /// It is a kind of teleport.
        /// </para>
        /// </summary>
        /// <param name="position">New position if defined. Otherwise don't change position.</param>
        /// <param name="rotation">New rotation if defined. Otherwise don't change rotation.</param>
        public void InstantRelocate(Vector3? position = null, Quaternion? rotation = null)
        {
            neutrinoEffect_.resetPosition(
                position.HasValue ? NeutrinoVec3(position.Value) : (NeutrinoParticles._math.vec3?)null,
                rotation.HasValue ? NeutrinoQuat(rotation.Value) : (NeutrinoParticles._math.quat?)null);
        }

        /// <summary>
        /// Simulates the effect.
        /// </summary>
        /// <param name="gameTime">Game time from current frame.</param>
        /// <param name="position">New position if defined. Otherwise don't change position.</param>
        /// <param name="rotation">New rotation if defined. Otherwise don't change rotation.</param>
        public void Update(GameTime gameTime, Vector3? position = null, Quaternion? rotation = null)
        {
            Update((float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f, position, rotation);
        }

        /// <summary>
        /// Simulates the effect.
        /// </summary>
        /// <param name="dt">Time in seconds.</param>
        /// <param name="position">New position if defined. Otherwise don't change position.</param>
        /// <param name="rotation">New rotation if defined. Otherwise don't change rotation.</param>
        public void Update(float dt, Vector3? position = null, Quaternion? rotation = null)
        {
            if (position != null)
                Position = position.Value;

            if (rotation != null)
                Rotation = rotation.Value;

            neutrinoEffect_.update(dt, NeutrinoVec3(Position), NeutrinoQuat(Rotation));
        }

        /// <summary>
        /// Renders the effect.
        /// 
        /// <para>
        /// When rendering several effects in a row, for additional optimizations use 
        /// <see cref="Context.BeginDraw()"/> and <see cref="Context.EndDraw()"/>.
        /// </para>
        /// 
        /// </summary>
        /// <param name="projMatrix">Projection matrix to render. By default 
        ///     <see cref="Context.CreateDefaultProjectionMatrix()" /> will be used.</param>
        /// <param name="viewMatrix">View matrix to render. By default
        ///     <see cref="Context.CreateDefaultViewMatrix()"/> will be used.</param>
        public void Draw(Matrix? projMatrix = null, Matrix? viewMatrix = null)
        {
            bool needsBeginEndRender = !renderer_.InRender;

            if (needsBeginEndRender)
                renderer_.BeginDraw();

            Matrix _viewMatrix = viewMatrix.HasValue ? viewMatrix.Value : Context.CreateDefaultViewMatrix();
            Matrix _projMatrix = projMatrix.HasValue ? projMatrix.Value : Context.CreateDefaultProjectionMatrix();

            renderer_.WorldViewProj = Matrix.Multiply(_viewMatrix, _projMatrix);

            Matrix cameraMatrix;
            Matrix.Invert(ref _viewMatrix, out cameraMatrix);

            NeutrinoParticles._math.vec3? renderModelPosition = null;
            NeutrinoParticles._math.quat? renderModelRotation = null;

            Vector3 cx = cameraMatrix.Right;
            Vector3 cy = Context.AxisYGoesUp ? cameraMatrix.Up : -cameraMatrix.Up;
            Vector3 cz = cameraMatrix.Backward;

            neutrinoEffect_.updateRenderBuffer(
                    NeutrinoParticles._math.vec3_(cx.X, cx.Y, cx.Z), // camera right vector
                    NeutrinoParticles._math.vec3_(cy.X, cy.Y, cy.Z), // camera up vector
                    NeutrinoParticles._math.vec3_(cz.X, cz.Y, cz.Z), // camera direction vector
                    renderModelPosition, renderModelRotation // position and rotation which will be used on render
                    );

            if (needsBeginEndRender)
                renderer_.EndDraw();
        }

        void NeutrinoParticles.RenderBuffer.initialize(uint maxNumVertices, uint[] texChannels, uint maxNumRenderCalls)
        {
        }

        void NeutrinoParticles.RenderBuffer.beginRenderCall(ushort renderStyleIndex)
        {
            var neutrinoModel = model_.NeutrinoModel;
            RenderStyle rs = neutrinoModel.renderStyles()[renderStyleIndex];
            RenderMaterial material = neutrinoModel.materials()[rs.material_];
            var texIndex = rs.textureIndex_[0];
            Texture2D texture = model_.Textures[texIndex];

            renderer_.BeginMaterial(material, texture);

            texCoordRemap_ = model_.TexCoordRemap[texIndex];
        }

        void NeutrinoParticles.RenderBuffer.pushGeometry(RenderVertex[] vertices, ushort[] indices)
        {
            var batchVertices = temp_;

            for (uint i = 0; i < vertices.Length; ++i)
            {
                var from = vertices[i];
                VertexPositionColorTexture to;

                to.Position.X = from.position_.x * Scale.X;
                to.Position.Y = from.position_.y * Scale.Y;
                to.Position.Z = from.position_.z * Scale.Z;

                to.Color = Color.FromNonPremultiplied(
                    (int)(from.color_ & 0xFF),
                    (int)((from.color_ >> 8) & 0xFF),
                    (int)((from.color_ >> 16) & 0xFF),
                    (int)((from.color_ >> 24) & 0xFF));

                if (texCoordRemap_ != null)
                {
                    to.TextureCoordinate.X = from.texCoords_[0][0] * texCoordRemap_.size.X
                        + texCoordRemap_.offset.X;
                    to.TextureCoordinate.Y = from.texCoords_[0][1] * texCoordRemap_.size.Y
                        + texCoordRemap_.offset.Y;
                }
                else
                {
                    to.TextureCoordinate.X = from.texCoords_[0][0];
                    to.TextureCoordinate.Y = from.texCoords_[0][1];
                }

                batchVertices[i] = to;
            }

            renderer_.BatchQuad(batchVertices);
        }

        void NeutrinoParticles.RenderBuffer.endRenderCall(ushort renderStyleIndex)
        {
        }

        void NeutrinoParticles.RenderBuffer.cleanup()
        {
        }

        private NeutrinoParticles._math.vec3 NeutrinoVec3(Vector3 v)
        {
            return NeutrinoParticles._math.vec3_(v.X / Scale.X, v.Y / Scale.Y, v.Z / Scale.Z);
        }

        private NeutrinoParticles._math.quat NeutrinoQuat(Quaternion q)
        {
            return NeutrinoParticles._math.quat_(q.W, q.X, q.Y, q.Z);
        }
    }
}
