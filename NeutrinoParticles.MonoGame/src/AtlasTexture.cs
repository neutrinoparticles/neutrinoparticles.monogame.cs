﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace NeutrinoParticles.MonoGame
{
    /// <summary>
    /// Describes a texture on atlas.
    /// <para>
    /// Consists of texture remapping object <see cref="texCoordRemap"/> 
    /// and atlas <see cref="texture"/> to render.
    /// </para>
    /// </summary>
    public class AtlasTexture
    {
        /// <summary>
        /// Placement of texture on atlas.
        /// </summary>
        public TexCoordRemap texCoordRemap;
        /// <summary>
        /// Atlas texture to render.
        /// </summary>
        public Texture2D texture;

        /// <summary>
        /// Creates description of texture on atlas.
        /// </summary>
        /// <param name="offset">Offset of texture quad on atlas. In range [0;1].</param>
        /// <param name="size">Size of texture quad on atlas. In range [0;1]</param>
        /// <param name="texture">Atlas texture to render.</param>
        public AtlasTexture(Vector2 offset, Vector2 size, Texture2D texture)
        {
            this.texCoordRemap = new TexCoordRemap(offset, size);
            this.texture = texture;
        }
    };
}
