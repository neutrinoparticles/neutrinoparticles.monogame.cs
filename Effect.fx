#if OPENGL

#define SV_POSITION POSITION
#define VS_SHADERMODEL vs_3_0
#define PS_SHADERMODEL ps_3_0

#else

#define VS_SHADERMODEL vs_4_0_level_9_1
#define PS_SHADERMODEL ps_4_0_level_9_1

#endif

Texture2D<float4> Texture;
sampler TextureSampler;

cbuffer Parameters
{
    float4x4 WorldViewProj;
};

struct VSInput
{
    float4 Position : SV_POSITION;
    float2 TexCoord : TEXCOORD;
    float4 Color : COLOR;
};

struct VSOutput
{
    float4 PositionPS : SV_POSITION;
    float4 Color : COLOR0;
    float2 TexCoord   : TEXCOORD0;
};

VSOutput VSDefault(VSInput vin)
{
    VSOutput vout;
  
    vout.PositionPS = mul(vin.Position, WorldViewProj);
    vout.TexCoord = float2(vin.TexCoord.x, 1.0 - vin.TexCoord.y);
    vout.Color = float4(vin.Color.rgb, vin.Color.a);
    
    return vout;
}

float4 PSDefault(VSOutput pin) : SV_TARGET0
{
	return Texture.Sample(TextureSampler, pin.TexCoord) * pin.Color;
}

float4 PSMultiply(VSOutput pin) : SV_TARGET0
{
	float4 texel = Texture.Sample(TextureSampler, pin.TexCoord);
	float alpha = pin.Color.a * texel.a;
	
	if (alpha > 0.01)
	{
		float3 rgb = pin.Color.rgb * texel.rgb / alpha;
		return float4(lerp(float3(1, 1, 1), rgb, alpha), 1);
	}
	else
	{
		return float4(1, 1, 1, 1);
	}
}

technique Default 
{ 
	pass { 
		VertexShader = compile VS_SHADERMODEL VSDefault(); 
		PixelShader = compile PS_SHADERMODEL PSDefault(); 
	} 
}

technique Multiply
{
	pass {
		VertexShader = compile VS_SHADERMODEL VSDefault(); 
		PixelShader = compile PS_SHADERMODEL PSMultiply(); 
	}
}

