## 3.8.14
* Fixed GL effect selector in the Context.

## 3.8.12
* Fixed re-creation of NeutrinoParticles.MonoGame.Context when noise generation is turned on.

## 3.8.11
* Renamed ParticleEffect.ResetPosition() -> ParticleEffect.InstantRelocate().

## 3.8.10
* IBCT reexported.
* Color IBCT added.
* Core 1.0.5 used.

## 3.8.9
* Particles color fix.
* Aligned to NeutrinoParticles.Core 1.0.5

## 3.8.8
* TexturesBasePath added to ParticleEffectModel. It overrides Context.TexturesBasePath if defined.

## 3.8.7
* Non-uniform scale for ParticleEffect.
* ParticleEffect.Reset() added for resetting the effect.

## 3.8.6
* NeutrinoParticles.MonoGame moved to separate folder to simplify adding the project to solution as source code.

## 3.8.5
* Added publishing of debugging symbols.

## 3.8.3
* Added publishing of debugging symbols.

## 3.8.2
* Trying to fix dependency version to pick the latest NeutrinoParticles.Core instead of 1.0.0.

## 3.8.0
* Version aligned to MonoGame.

## 0.0.9
* Moved to .NET Framework 4.5
