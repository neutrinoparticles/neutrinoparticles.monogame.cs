@echo off
setlocal

dotnet new tool-manifest
dotnet tool install dotnet-mgfxc --version 3.8.0.1641
dotnet mgfxc Effect.fx NeutrinoParticles.MonoGame.GL\Resources\Effect.mgfxo
dotnet mgfxc Effect.fx NeutrinoParticles.MonoGame.DX\Resources\Effect.mgfxo /Profile:DirectX_11

endlocal
