// 7a49ddcb-ee60-48d1-9056-61aca1022368

#pragma warning disable 219

using System;

namespace NeutrinoParticles
{
	public class texture_grid : EffectModel
	{
		public class Emitter__1 : EmitterModel
		{
			public class ParticleImpl : RenderableParticle
			{
				public float _lifetime;
				public _math.vec3 _Position;
				public _math.vec3 _Velocity;
				public float _Angle;
				public override _math.vec2 origin() { return _math.vec2_(0.5F,0.5F); }
				public override float angle() { return _Angle; }
				public override _math.quat rotation() { return _math.quat_(1, 0, 0, 0); }
				public float size1_;
				public override float size1() { return size1_; }
				public override _math.vec2 size2() { return _math.vec2_(0, 0); }
				public override _math.vec3 color() { return _math.vec3_(1F,1F,1F); }
				public override float alpha() { return 1F; }
				public float gridIndex_;
				public override float gridIndex() { return gridIndex_; }
				public override AttachedEmitter[] attachedEmitters() { return null; }

			}


			public class EmitterData
			{
			}

			public class GeneratorImpl : GeneratorPeriodic.Impl
			{
				public float burst() { return 1F; }
				public float? fixedTime() { return null; }
				public float? fixedShots() { return null; }
				public float startPhase() { return 1F; }
				public float rate() { return 15F; }
			}

			_math.vec2 [][,] _path =
			{
				new _math.vec2[,] {{_math.vec2_(-101F,-99.5F)},{_math.vec2_(-101F,-99.5F)},{_math.vec2_(-101F,-99.5F)}}
			};

			public class ConstructorImpl : ConstructorQuads.Impl
			{
				public ConstructorQuads.RotationType rotationType() { return ConstructorQuads.RotationType.Faced; }
				public ConstructorQuads.SizeType sizeType() { return ConstructorQuads.SizeType.Quad; }
				public ConstructorQuads.TexMapType texMapType() { return ConstructorQuads.TexMapType.Grid; }
				public _math.vec2 gridSize() { return _math.vec2_(2, 2); }
				public ushort renderStyleIndex() { return 0; }
			}


			public override void updateEmitter(Emitter emitter)
			{
				EmitterData emitterData = (EmitterData)emitter.data();
				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
			}

			public override void initParticle(Emitter emitter, Particle particle)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*1F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void initBurstParticle(Emitter emitter, Particle particle, Particle first)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				ParticleImpl firstImpl = (ParticleImpl)first;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*1F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void updateParticle(Emitter emitter, Particle particle, float dt)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				bool toTerminate = false;
				particleImpl._lifetime += dt;
				_math.vec3 move_ = _math.addv3_(particleImpl._Position, _math.mulv3scalar_(particleImpl._Velocity, dt));
				particleImpl._Position = move_;
				particle.position_ = particleImpl._Position;
				float value_ = 2F;
				toTerminate |= (particleImpl._lifetime > value_);
				float value_a = 30F;
				float value_b = 0F;
				particleImpl.size1_ = value_a;
				particleImpl.gridIndex_ = value_b;
				if (toTerminate)
				{
					particle.dead_ = true;
				}
			}
			public Emitter__1()
			{
				generatorCreator_ = (Emitter emitter) => { return new GeneratorPeriodic(emitter, new GeneratorImpl()); };
				constructorCreator_ = (Emitter emitter) => { return new ConstructorQuads(emitter, new ConstructorImpl()); };
				name_ = "1";
				maxNumParticles_ = 100;
				sorting_ = Emitter.Sorting.OldToYoung;
				particleCreator_ = (Effect effect) => { return new ParticleImpl(); };
				emitterDataCreator_ = () => { return new EmitterData(); };
			}
		}

		public class Emitter__2 : EmitterModel
		{
			public class ParticleImpl : RenderableParticle
			{
				public float _lifetime;
				public _math.vec3 _Position;
				public _math.vec3 _Velocity;
				public float _Angle;
				public override _math.vec2 origin() { return _math.vec2_(0.5F,0.5F); }
				public override float angle() { return _Angle; }
				public override _math.quat rotation() { return _math.quat_(1, 0, 0, 0); }
				public float size1_;
				public override float size1() { return size1_; }
				public override _math.vec2 size2() { return _math.vec2_(0, 0); }
				public override _math.vec3 color() { return _math.vec3_(1F,1F,1F); }
				public override float alpha() { return 1F; }
				public float gridIndex_;
				public override float gridIndex() { return gridIndex_; }
				public override AttachedEmitter[] attachedEmitters() { return null; }

			}


			public class EmitterData
			{
			}

			public class GeneratorImpl : GeneratorPeriodic.Impl
			{
				public float burst() { return 1F; }
				public float? fixedTime() { return null; }
				public float? fixedShots() { return null; }
				public float startPhase() { return 1F; }
				public float rate() { return 15F; }
			}

			_math.vec2 [][,] _path =
			{
				new _math.vec2[,] {{_math.vec2_(98F,-101.5F)},{_math.vec2_(98F,-101.5F)},{_math.vec2_(98F,-101.5F)}}
			};

			public class ConstructorImpl : ConstructorQuads.Impl
			{
				public ConstructorQuads.RotationType rotationType() { return ConstructorQuads.RotationType.Faced; }
				public ConstructorQuads.SizeType sizeType() { return ConstructorQuads.SizeType.Quad; }
				public ConstructorQuads.TexMapType texMapType() { return ConstructorQuads.TexMapType.Grid; }
				public _math.vec2 gridSize() { return _math.vec2_(2, 2); }
				public ushort renderStyleIndex() { return 0; }
			}


			public override void updateEmitter(Emitter emitter)
			{
				EmitterData emitterData = (EmitterData)emitter.data();
				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
			}

			public override void initParticle(Emitter emitter, Particle particle)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*1F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void initBurstParticle(Emitter emitter, Particle particle, Particle first)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				ParticleImpl firstImpl = (ParticleImpl)first;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*1F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void updateParticle(Emitter emitter, Particle particle, float dt)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				bool toTerminate = false;
				particleImpl._lifetime += dt;
				_math.vec3 move_ = _math.addv3_(particleImpl._Position, _math.mulv3scalar_(particleImpl._Velocity, dt));
				particleImpl._Position = move_;
				particle.position_ = particleImpl._Position;
				float value_ = 2F;
				toTerminate |= (particleImpl._lifetime > value_);
				float value_a = 30F;
				float value_b = 1F;
				particleImpl.size1_ = value_a;
				particleImpl.gridIndex_ = value_b;
				if (toTerminate)
				{
					particle.dead_ = true;
				}
			}
			public Emitter__2()
			{
				generatorCreator_ = (Emitter emitter) => { return new GeneratorPeriodic(emitter, new GeneratorImpl()); };
				constructorCreator_ = (Emitter emitter) => { return new ConstructorQuads(emitter, new ConstructorImpl()); };
				name_ = "2";
				maxNumParticles_ = 100;
				sorting_ = Emitter.Sorting.OldToYoung;
				particleCreator_ = (Effect effect) => { return new ParticleImpl(); };
				emitterDataCreator_ = () => { return new EmitterData(); };
			}
		}

		public class Emitter__3 : EmitterModel
		{
			public class ParticleImpl : RenderableParticle
			{
				public float _lifetime;
				public _math.vec3 _Position;
				public _math.vec3 _Velocity;
				public float _Angle;
				public override _math.vec2 origin() { return _math.vec2_(0.5F,0.5F); }
				public override float angle() { return _Angle; }
				public override _math.quat rotation() { return _math.quat_(1, 0, 0, 0); }
				public float size1_;
				public override float size1() { return size1_; }
				public override _math.vec2 size2() { return _math.vec2_(0, 0); }
				public override _math.vec3 color() { return _math.vec3_(1F,1F,1F); }
				public override float alpha() { return 1F; }
				public float gridIndex_;
				public override float gridIndex() { return gridIndex_; }
				public override AttachedEmitter[] attachedEmitters() { return null; }

			}


			public class EmitterData
			{
			}

			public class GeneratorImpl : GeneratorPeriodic.Impl
			{
				public float burst() { return 1F; }
				public float? fixedTime() { return null; }
				public float? fixedShots() { return null; }
				public float startPhase() { return 1F; }
				public float rate() { return 15F; }
			}

			_math.vec2 [][,] _path =
			{
				new _math.vec2[,] {{_math.vec2_(-102F,100.5F)},{_math.vec2_(-102F,100.5F)},{_math.vec2_(-102F,100.5F)}}
			};

			public class ConstructorImpl : ConstructorQuads.Impl
			{
				public ConstructorQuads.RotationType rotationType() { return ConstructorQuads.RotationType.Faced; }
				public ConstructorQuads.SizeType sizeType() { return ConstructorQuads.SizeType.Quad; }
				public ConstructorQuads.TexMapType texMapType() { return ConstructorQuads.TexMapType.Grid; }
				public _math.vec2 gridSize() { return _math.vec2_(2, 2); }
				public ushort renderStyleIndex() { return 0; }
			}


			public override void updateEmitter(Emitter emitter)
			{
				EmitterData emitterData = (EmitterData)emitter.data();
				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
			}

			public override void initParticle(Emitter emitter, Particle particle)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*1F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void initBurstParticle(Emitter emitter, Particle particle, Particle first)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				ParticleImpl firstImpl = (ParticleImpl)first;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*1F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void updateParticle(Emitter emitter, Particle particle, float dt)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				bool toTerminate = false;
				particleImpl._lifetime += dt;
				_math.vec3 move_ = _math.addv3_(particleImpl._Position, _math.mulv3scalar_(particleImpl._Velocity, dt));
				particleImpl._Position = move_;
				particle.position_ = particleImpl._Position;
				float value_ = 2F;
				toTerminate |= (particleImpl._lifetime > value_);
				float value_a = 30F;
				float value_b = 2F;
				particleImpl.size1_ = value_a;
				particleImpl.gridIndex_ = value_b;
				if (toTerminate)
				{
					particle.dead_ = true;
				}
			}
			public Emitter__3()
			{
				generatorCreator_ = (Emitter emitter) => { return new GeneratorPeriodic(emitter, new GeneratorImpl()); };
				constructorCreator_ = (Emitter emitter) => { return new ConstructorQuads(emitter, new ConstructorImpl()); };
				name_ = "3";
				maxNumParticles_ = 100;
				sorting_ = Emitter.Sorting.OldToYoung;
				particleCreator_ = (Effect effect) => { return new ParticleImpl(); };
				emitterDataCreator_ = () => { return new EmitterData(); };
			}
		}

		public class Emitter__4 : EmitterModel
		{
			public class ParticleImpl : RenderableParticle
			{
				public float _lifetime;
				public _math.vec3 _Position;
				public _math.vec3 _Velocity;
				public float _Angle;
				public override _math.vec2 origin() { return _math.vec2_(0.5F,0.5F); }
				public override float angle() { return _Angle; }
				public override _math.quat rotation() { return _math.quat_(1, 0, 0, 0); }
				public float size1_;
				public override float size1() { return size1_; }
				public override _math.vec2 size2() { return _math.vec2_(0, 0); }
				public override _math.vec3 color() { return _math.vec3_(1F,1F,1F); }
				public override float alpha() { return 1F; }
				public float gridIndex_;
				public override float gridIndex() { return gridIndex_; }
				public override AttachedEmitter[] attachedEmitters() { return null; }

			}


			public class EmitterData
			{
			}

			public class GeneratorImpl : GeneratorPeriodic.Impl
			{
				public float burst() { return 1F; }
				public float? fixedTime() { return null; }
				public float? fixedShots() { return null; }
				public float startPhase() { return 1F; }
				public float rate() { return 15F; }
			}

			_math.vec2 [][,] _path =
			{
				new _math.vec2[,] {{_math.vec2_(100F,101.5F)},{_math.vec2_(100F,101.5F)},{_math.vec2_(100F,101.5F)}}
			};

			public class ConstructorImpl : ConstructorQuads.Impl
			{
				public ConstructorQuads.RotationType rotationType() { return ConstructorQuads.RotationType.Faced; }
				public ConstructorQuads.SizeType sizeType() { return ConstructorQuads.SizeType.Quad; }
				public ConstructorQuads.TexMapType texMapType() { return ConstructorQuads.TexMapType.Grid; }
				public _math.vec2 gridSize() { return _math.vec2_(2, 2); }
				public ushort renderStyleIndex() { return 0; }
			}


			public override void updateEmitter(Emitter emitter)
			{
				EmitterData emitterData = (EmitterData)emitter.data();
				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
			}

			public override void initParticle(Emitter emitter, Particle particle)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*1F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void initBurstParticle(Emitter emitter, Particle particle, Particle first)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				ParticleImpl firstImpl = (ParticleImpl)first;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*1F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void updateParticle(Emitter emitter, Particle particle, float dt)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				bool toTerminate = false;
				particleImpl._lifetime += dt;
				_math.vec3 move_ = _math.addv3_(particleImpl._Position, _math.mulv3scalar_(particleImpl._Velocity, dt));
				particleImpl._Position = move_;
				particle.position_ = particleImpl._Position;
				float value_ = 2F;
				toTerminate |= (particleImpl._lifetime > value_);
				float value_a = 30F;
				float value_b = 3F;
				particleImpl.size1_ = value_a;
				particleImpl.gridIndex_ = value_b;
				if (toTerminate)
				{
					particle.dead_ = true;
				}
			}
			public Emitter__4()
			{
				generatorCreator_ = (Emitter emitter) => { return new GeneratorPeriodic(emitter, new GeneratorImpl()); };
				constructorCreator_ = (Emitter emitter) => { return new ConstructorQuads(emitter, new ConstructorImpl()); };
				name_ = "4";
				maxNumParticles_ = 100;
				sorting_ = Emitter.Sorting.OldToYoung;
				particleCreator_ = (Effect effect) => { return new ParticleImpl(); };
				emitterDataCreator_ = () => { return new EmitterData(); };
			}
		}

		public texture_grid()
		{
			textures_ = new string[] { "grid2x2.png" };
			materials_ = new RenderMaterial[] { RenderMaterial.Normal };
			renderStyles_ = new RenderStyle[] { new RenderStyle(0,new uint[] {0}) };
			frameTime_ = 0.0333333F;
			presimulateTime_ = 0F;
			maxNumRenderCalls_ = 400;
			maxNumParticles_ = 400;
			emitterModels_ = new EmitterModel[]{ new Emitter__1(), new Emitter__2(), new Emitter__3(), new Emitter__4() };
			activeEmitterModels_ = new uint[] { 0, 1, 2, 3 };
			randomGeneratorCreator_ = () => { Taus88 taus88 = new Taus88(0); return taus88.rand; };
		}
	}
}

#pragma warning restore 219