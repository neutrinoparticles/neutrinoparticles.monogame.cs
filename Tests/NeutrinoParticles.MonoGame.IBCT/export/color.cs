// 7205a5c4-0617-4b23-907f-34853ffa3161

#pragma warning disable 219

using System;

namespace NeutrinoParticles
{
	public class color : EffectModel
	{
		public class Emitter_DefaultEmitter : EmitterModel
		{
			public class ParticleImpl : RenderableParticle
			{
				public float _lifetime;
				public _math.vec3 _Position;
				public _math.vec3 _Velocity;
				public float _Angle;
				public _math.vec3 _Color;
				public override _math.vec2 origin() { return _math.vec2_(0.5F,0.5F); }
				public override float angle() { return _Angle; }
				public override _math.quat rotation() { return _math.quat_(1, 0, 0, 0); }
				public float size1_;
				public override float size1() { return size1_; }
				public override _math.vec2 size2() { return _math.vec2_(0, 0); }
				public override _math.vec3 color() { return _Color; }
				public override float alpha() { return 1F; }
				public override float gridIndex() { return 0; }
				public override AttachedEmitter[] attachedEmitters() { return null; }

			}


			public class EmitterData
			{
			}

			public class GeneratorImpl : GeneratorPeriodic.Impl
			{
				public float burst() { return 1F; }
				public float? fixedTime() { return null; }
				public float? fixedShots() { return null; }
				public float startPhase() { return 1F; }
				public float rate() { return 5F; }
			}

			float [][][] _plot =
			{
				new float[][] { new float[]{ 1F,1F,1F }, new float[]{ 0F,0F,0F }, new float[]{ 1F,1F,1F }},
				new float[][] { new float[]{ 0F,0F,0F }, new float[]{ 1F,1F,1F }, new float[]{ 0F,0F,0F }},
				new float[][] { new float[]{ 0F,0F,0F }, new float[]{ 1F,1F,1F }}
			};

			public class ConstructorImpl : ConstructorQuads.Impl
			{
				public ConstructorQuads.RotationType rotationType() { return ConstructorQuads.RotationType.Faced; }
				public ConstructorQuads.SizeType sizeType() { return ConstructorQuads.SizeType.Quad; }
				public ConstructorQuads.TexMapType texMapType() { return ConstructorQuads.TexMapType.WholeRect; }
				public _math.vec2 gridSize() { return _math.vec2_(0, 0); }
				public ushort renderStyleIndex() { return 0; }
			}


			public override void updateEmitter(Emitter emitter)
			{
				EmitterData emitterData = (EmitterData)emitter.data();
				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
			}

			public override void initParticle(Emitter emitter, Particle particle)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				_math.vec3 value_ = _math.vec3_(0F, 0F, 0F);
				particleImpl._Position = _math.addv3_(value_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				_math.vec3 _plot_out;
				float _plot_in0=(rnd_<0F?0F:(rnd_>0.977187F?0.977187F:rnd_));
				_math.PathRes _plot_srch0 = _plot_in0<0.976187?_plot_in0<0.3?_math.pathRes(0,(_plot_in0-0F)*3.33333F):_math.pathRes(1,(_plot_in0-0.3F)*1.47888F):_math.pathRes(2,(_plot_in0-0.976187F)*1000F);
				_math.funcLerp(out _plot_out.x, this._plot[0][_plot_srch0.s],_plot_srch0.i);
				float _plot_in1=(rnd_<0F?0F:(rnd_>1F?1F:rnd_));
				_math.PathRes _plot_srch1 = _plot_in1<0.6?_plot_in1<0.3?_math.pathRes(0,(_plot_in1-0F)*3.33333F):_math.pathRes(1,(_plot_in1-0.3F)*3.33333F):_math.pathRes(2,(_plot_in1-0.6F)*2.5F);
				_math.funcLerp(out _plot_out.y, this._plot[1][_plot_srch1.s],_plot_srch1.i);
				float _plot_in2=(rnd_<0F?0F:(rnd_>1F?1F:rnd_));
				_math.PathRes _plot_srch2 = _plot_in2<0.6?_math.pathRes(0,(_plot_in2-0F)*1.66667F):_math.pathRes(1,(_plot_in2-0.6F)*2.5F);
				_math.funcLerp(out _plot_out.z, this._plot[2][_plot_srch2.s],_plot_srch2.i);
				particleImpl._Color = _plot_out;
				particle.position_ = particleImpl._Position;
			}

			public override void initBurstParticle(Emitter emitter, Particle particle, Particle first)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				ParticleImpl firstImpl = (ParticleImpl)first;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				_math.vec3 value_ = _math.vec3_(0F, 0F, 0F);
				particleImpl._Position = _math.addv3_(value_, emitter.position());
				_math.vec3 randvec_ = _math.randv3gen_(100F, emitter.random());
				particleImpl._Velocity = randvec_;
				particleImpl._Angle = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				_math.vec3 _plot_out;
				float _plot_in0=(rnd_<0F?0F:(rnd_>0.977187F?0.977187F:rnd_));
				_math.PathRes _plot_srch0 = _plot_in0<0.976187?_plot_in0<0.3?_math.pathRes(0,(_plot_in0-0F)*3.33333F):_math.pathRes(1,(_plot_in0-0.3F)*1.47888F):_math.pathRes(2,(_plot_in0-0.976187F)*1000F);
				_math.funcLerp(out _plot_out.x, this._plot[0][_plot_srch0.s],_plot_srch0.i);
				float _plot_in1=(rnd_<0F?0F:(rnd_>1F?1F:rnd_));
				_math.PathRes _plot_srch1 = _plot_in1<0.6?_plot_in1<0.3?_math.pathRes(0,(_plot_in1-0F)*3.33333F):_math.pathRes(1,(_plot_in1-0.3F)*3.33333F):_math.pathRes(2,(_plot_in1-0.6F)*2.5F);
				_math.funcLerp(out _plot_out.y, this._plot[1][_plot_srch1.s],_plot_srch1.i);
				float _plot_in2=(rnd_<0F?0F:(rnd_>1F?1F:rnd_));
				_math.PathRes _plot_srch2 = _plot_in2<0.6?_math.pathRes(0,(_plot_in2-0F)*1.66667F):_math.pathRes(1,(_plot_in2-0.6F)*2.5F);
				_math.funcLerp(out _plot_out.z, this._plot[2][_plot_srch2.s],_plot_srch2.i);
				particleImpl._Color = _plot_out;
				particle.position_ = particleImpl._Position;
			}

			public override void updateParticle(Emitter emitter, Particle particle, float dt)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				bool toTerminate = false;
				particleImpl._lifetime += dt;
				_math.vec3 move_ = _math.addv3_(particleImpl._Position, _math.mulv3scalar_(particleImpl._Velocity, dt));
				particleImpl._Position = move_;
				particle.position_ = particleImpl._Position;
				float value_ = 2F;
				toTerminate |= (particleImpl._lifetime > value_);
				float value_a = 30F;
				particleImpl.size1_ = value_a;
				if (toTerminate)
				{
					particle.dead_ = true;
				}
			}
			public Emitter_DefaultEmitter()
			{
				generatorCreator_ = (Emitter emitter) => { return new GeneratorPeriodic(emitter, new GeneratorImpl()); };
				constructorCreator_ = (Emitter emitter) => { return new ConstructorQuads(emitter, new ConstructorImpl()); };
				name_ = "DefaultEmitter";
				maxNumParticles_ = 100;
				sorting_ = Emitter.Sorting.OldToYoung;
				particleCreator_ = (Effect effect) => { return new ParticleImpl(); };
				emitterDataCreator_ = () => { return new EmitterData(); };
			}
		}

		public color()
		{
			textures_ = new string[] { "test0.png" };
			materials_ = new RenderMaterial[] { RenderMaterial.Normal };
			renderStyles_ = new RenderStyle[] { new RenderStyle(0,new uint[] {0}) };
			frameTime_ = 0.0333333F;
			presimulateTime_ = 0F;
			maxNumRenderCalls_ = 100;
			maxNumParticles_ = 100;
			emitterModels_ = new EmitterModel[]{ new Emitter_DefaultEmitter() };
			activeEmitterModels_ = new uint[] { 0 };
			randomGeneratorCreator_ = () => { Taus88 taus88 = new Taus88(0); return taus88.rand; };
		}
	}
}

#pragma warning restore 219