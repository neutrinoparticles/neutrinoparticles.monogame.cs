// 92d0441b-4aca-476b-872f-57ed22ca44cf

#pragma warning disable 219

using System;

namespace NeutrinoParticles
{
	public class simulation_startpos_path : EffectModel
	{
		public class Emitter_DefaultEmitter : EmitterModel
		{
			public class ParticleImpl : RenderableParticle
			{
				public float _lifetime;
				public _math.vec3 _Position;
				public float _Angle;
				public override _math.vec2 origin() { return _math.vec2_(0.5F,0.5F); }
				public override float angle() { return _Angle; }
				public override _math.quat rotation() { return _math.quat_(1, 0, 0, 0); }
				public float size1_;
				public override float size1() { return size1_; }
				public override _math.vec2 size2() { return _math.vec2_(0, 0); }
				public override _math.vec3 color() { return _math.vec3_(1F,1F,1F); }
				public override float alpha() { return 1F; }
				public override float gridIndex() { return 0; }
				public override AttachedEmitter[] attachedEmitters() { return null; }

			}


			public class EmitterData
			{
			}

			public class GeneratorImpl : GeneratorPeriodic.Impl
			{
				public float burst() { return 1F; }
				public float? fixedTime() { return null; }
				public float? fixedShots() { return null; }
				public float startPhase() { return 1F; }
				public float rate() { return 50F; }
			}

			_math.vec2 [][,] _path =
			{
				new _math.vec2[,] {{_math.vec2_(-200F,0F)},{_math.vec2_(-100F,-202F)},{_math.vec2_(-100F,-202F)}},
				new _math.vec2[,] {{_math.vec2_(-100F,-202F)},{_math.vec2_(-92.7306F,-170.221F)},{_math.vec2_(-84.5118F,-138.682F)},{_math.vec2_(-75.1593F,-107.462F)},{_math.vec2_(-64.4275F,-76.6872F)},{_math.vec2_(-51.9835F,-46.5705F)},{_math.vec2_(-37.3549F,-17.4531F)},{_math.vec2_(-19.8799F,10.0368F)},{_math.vec2_(1.33874F,34.7238F)},{_math.vec2_(27.2242F,54.3906F)},{_math.vec2_(57.6542F,65.6396F)},{_math.vec2_(90.0953F,66.084F)},{_math.vec2_(121.322F,57.0512F)},{_math.vec2_(149.985F,41.6189F)},{_math.vec2_(176.093F,22.1385F)},{_math.vec2_(200F,-3.51126e-05F)},{_math.vec2_(200F,-3.51126e-05F)}},
				new _math.vec2[,] {{_math.vec2_(201F,-161F)},{_math.vec2_(22F,-251F)},{_math.vec2_(22F,-251F)}}
			};

			public class ConstructorImpl : ConstructorQuads.Impl
			{
				public ConstructorQuads.RotationType rotationType() { return ConstructorQuads.RotationType.Faced; }
				public ConstructorQuads.SizeType sizeType() { return ConstructorQuads.SizeType.Quad; }
				public ConstructorQuads.TexMapType texMapType() { return ConstructorQuads.TexMapType.WholeRect; }
				public _math.vec2 gridSize() { return _math.vec2_(0, 0); }
				public ushort renderStyleIndex() { return 0; }
			}


			public override void updateEmitter(Emitter emitter)
			{
				EmitterData emitterData = (EmitterData)emitter.data();
				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
			}

			public override void initParticle(Emitter emitter, Particle particle)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _path_in<0.780805?_path_in<0.246595?_math.pathRes(0,(_path_in-0F)*4.05523F):_math.pathRes(1,(_path_in-0.246595F)*28.0788F):_math.pathRes(2,(_path_in-0.780805F)*4.56215F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void initBurstParticle(Emitter emitter, Particle particle, Particle first)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				ParticleImpl firstImpl = (ParticleImpl)first;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				float rnd_ = 0F + emitter.random()() * (1F - 0F);
				float _path_in = _math.clamp_(rnd_, 0, 1);
				_math.PathRes _path_srch = _path_in<0.780805?_path_in<0.246595?_math.pathRes(0,(_path_in-0F)*4.05523F):_math.pathRes(1,(_path_in-0.246595F)*28.0788F):_math.pathRes(2,(_path_in-0.780805F)*4.56215F);
				_math.vec2 _path_pos;
				_math.pathLerp1(out _path_pos, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				particleImpl._Position = _math.addv3_(conv3d_, emitter.position());
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void updateParticle(Emitter emitter, Particle particle, float dt)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				bool toTerminate = false;
				particleImpl._lifetime += dt;
				particle.position_ = particleImpl._Position;
				float value_ = 2F;
				toTerminate |= (particleImpl._lifetime > value_);
				float value_a = 30F;
				particleImpl.size1_ = value_a;
				if (toTerminate)
				{
					particle.dead_ = true;
				}
			}
			public Emitter_DefaultEmitter()
			{
				generatorCreator_ = (Emitter emitter) => { return new GeneratorPeriodic(emitter, new GeneratorImpl()); };
				constructorCreator_ = (Emitter emitter) => { return new ConstructorQuads(emitter, new ConstructorImpl()); };
				name_ = "DefaultEmitter";
				maxNumParticles_ = 100;
				sorting_ = Emitter.Sorting.OldToYoung;
				particleCreator_ = (Effect effect) => { return new ParticleImpl(); };
				emitterDataCreator_ = () => { return new EmitterData(); };
			}
		}

		public simulation_startpos_path()
		{
			textures_ = new string[] { "test0.png" };
			materials_ = new RenderMaterial[] { RenderMaterial.Normal };
			renderStyles_ = new RenderStyle[] { new RenderStyle(0,new uint[] {0}) };
			frameTime_ = 0.0333333F;
			presimulateTime_ = 0F;
			maxNumRenderCalls_ = 100;
			maxNumParticles_ = 100;
			emitterModels_ = new EmitterModel[]{ new Emitter_DefaultEmitter() };
			activeEmitterModels_ = new uint[] { 0 };
			randomGeneratorCreator_ = () => { Taus88 taus88 = new Taus88(0); return taus88.rand; };
		}
	}
}

#pragma warning restore 219