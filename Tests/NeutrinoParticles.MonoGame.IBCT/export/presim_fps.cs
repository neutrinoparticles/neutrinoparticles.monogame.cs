// 4c2a1a62-08d9-4852-b74f-72941898c803

#pragma warning disable 219

using System;

namespace NeutrinoParticles
{
	public class Effect_presim_fps : EffectModel
	{
		public class Emitter_Children : EmitterModel
		{
			public class ParticleImpl : RenderableParticle
			{
				public float _lifetime;
				public _math.vec3 _Position;
				public float _Angle;
				public override _math.vec2 origin() { return _math.vec2_(0.5F,0.5F); }
				public override float angle() { return _Angle; }
				public override _math.quat rotation() { return _math.quat_(1, 0, 0, 0); }
				public float size1_;
				public override float size1() { return size1_; }
				public override _math.vec2 size2() { return _math.vec2_(0, 0); }
				public override _math.vec3 color() { return _math.vec3_(1F,1F,1F); }
				public override float alpha() { return 1F; }
				public override float gridIndex() { return 0; }
				public override AttachedEmitter[] attachedEmitters() { return null; }

			}


			public class EmitterData
			{
			}

			public class GeneratorImpl : GeneratorDistance.Impl
			{
				public float burst() { return 1F; }
				public float startPhase() { return 1F; }
				public float segment() { return 5F; }
			}

			public class ConstructorImpl : ConstructorQuads.Impl
			{
				public ConstructorQuads.RotationType rotationType() { return ConstructorQuads.RotationType.Faced; }
				public ConstructorQuads.SizeType sizeType() { return ConstructorQuads.SizeType.Quad; }
				public ConstructorQuads.TexMapType texMapType() { return ConstructorQuads.TexMapType.WholeRect; }
				public _math.vec2 gridSize() { return _math.vec2_(0, 0); }
				public ushort renderStyleIndex() { return 0; }
			}


			public override void updateEmitter(Emitter emitter)
			{
				EmitterData emitterData = (EmitterData)emitter.data();
				GeneratorDistance generator = (GeneratorDistance)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
			}

			public override void initParticle(Emitter emitter, Particle particle)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorDistance generator = (GeneratorDistance)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				_math.vec3 value_ = _math.vec3_(0F, 0F, 0F);
				particleImpl._Position = _math.addv3_(value_, emitter.position());
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void initBurstParticle(Emitter emitter, Particle particle, Particle first)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				ParticleImpl firstImpl = (ParticleImpl)first;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorDistance generator = (GeneratorDistance)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				_math.vec3 value_ = _math.vec3_(0F, 0F, 0F);
				particleImpl._Position = _math.addv3_(value_, emitter.position());
				particleImpl._Angle = 0F;
				particle.position_ = particleImpl._Position;
			}

			public override void updateParticle(Emitter emitter, Particle particle, float dt)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorDistance generator = (GeneratorDistance)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				bool toTerminate = false;
				particleImpl._lifetime += dt;
				particle.position_ = particleImpl._Position;
				float value_ = 1F;
				toTerminate |= (particleImpl._lifetime > value_);
				float value_a = 10F;
				particleImpl.size1_ = value_a;
				if (toTerminate)
				{
					particle.dead_ = true;
				}
			}
			public Emitter_Children()
			{
				generatorCreator_ = (Emitter emitter) => { return new GeneratorDistance(emitter, new GeneratorImpl()); };
				constructorCreator_ = (Emitter emitter) => { return new ConstructorQuads(emitter, new ConstructorImpl()); };
				name_ = "Children";
				maxNumParticles_ = 100;
				sorting_ = Emitter.Sorting.OldToYoung;
				particleCreator_ = (Effect effect) => { return new ParticleImpl(); };
				emitterDataCreator_ = () => { return new EmitterData(); };
			}
		}

		public class Emitter_DefaultEmitter : EmitterModel
		{
			public class ParticleImpl : Particle
			{
				public float _lifetime;
				public _math.vec3 _Position;
				public _math.quat _Emitter_rot_;
				public float _Angle;
				public AttachedEmitter[] attachedEmitters_;
				public override AttachedEmitter[] attachedEmitters() { return attachedEmitters_; }

			}


			public class EmitterData
			{
			}

			public class GeneratorImpl : GeneratorPeriodic.Impl
			{
				public float burst() { return 1F; }
				public float? fixedTime() { return null; }
				public float? fixedShots() { return null; }
				public float startPhase() { return 1F; }
				public float rate() { return 0.5F; }
			}

			float [][][] _plot =
			{
				new float[][] { new float[]{ 0F,1F,1F }}
			};

			_math.vec2 [][,] _path =
			{
				new _math.vec2[,] {{_math.vec2_(110F,-1F),_math.vec2_(-0.120297F,-0.992738F)},{_math.vec2_(107.442F,-22.109F),_math.vec2_(-0.324023F,-0.946049F)},{_math.vec2_(100.555F,-42.2174F),_math.vec2_(-0.526414F,-0.850229F)},{_math.vec2_(89.3673F,-60.2869F),_math.vec2_(-0.701505F,-0.712665F)},{_math.vec2_(74.4565F,-75.4349F),_math.vec2_(-0.834941F,-0.550339F)},{_math.vec2_(56.7046F,-87.1358F),_math.vec2_(-0.926033F,-0.377442F)},{_math.vec2_(37.0116F,-95.1625F),_math.vec2_(-0.979803F,-0.199964F)},{_math.vec2_(16.173F,-99.4154F),_math.vec2_(-0.999844F,-0.0176646F)},{_math.vec2_(-5.09101F,-99.7911F),_math.vec2_(-0.985142F,0.17174F)},{_math.vec2_(-26.0388F,-96.1392F),_math.vec2_(-0.92988F,0.367862F)},{_math.vec2_(-45.8059F,-88.3193F),_math.vec2_(-0.826936F,0.562295F)},{_math.vec2_(-63.3797F,-76.3696F),_math.vec2_(-0.67673F,0.736232F)},{_math.vec2_(-77.7605F,-60.7243F),_math.vec2_(-0.49572F,0.868483F)},{_math.vec2_(-88.2976F,-42.2637F),_math.vec2_(-0.311168F,0.950355F)},{_math.vec2_(-94.9147F,-22.054F),_math.vec2_(-0.144992F,0.989433F)},{_math.vec2_(-98F,-1F),_math.vec2_(-0.144992F,0.989433F)},{_math.vec2_(-98F,-1F),_math.vec2_(-0.144992F,0.989433F)}}
			};


			public override void updateEmitter(Emitter emitter)
			{
				EmitterData emitterData = (EmitterData)emitter.data();
				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
			}

			public override void initParticle(Emitter emitter, Particle particle)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				_math.vec3 value_ = _math.vec3_(0F, 0F, 0F);
				particleImpl._Position = _math.addv3_(value_, emitter.position());
				particleImpl._Emitter_rot_ = emitter.rotation();
				particleImpl._Angle = 0F;
				float value_a = 2F;
				float expr_ = (particleImpl._lifetime / value_a);
				float _plot_out;
				float _plot_in0=(expr_<0F?0F:(expr_>1F?1F:expr_));
				_math.PathRes _plot_srch0 = _math.pathRes(0,(_plot_in0-0F)*1F);
				_math.funcLerp(out _plot_out, this._plot[0][_plot_srch0.s],_plot_srch0.i);
				float _path_in = _math.clamp_(_plot_out, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*15F);
				_math.vec2 _path_pos;
				_math.vec2 _path_dir;
				_math.pathLerp2(out _path_pos, out _path_dir, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				_math.vec3 trn3op0 = _math.applyv3quat_(conv3d_, particleImpl._Emitter_rot_);
				_math.addv3(out trn3op0, trn3op0, _math.vec3_(0F,0F,0F));
				_math.vec3 expr_a = _math.addv3_(particleImpl._Position, trn3op0);
				particle.position_ = expr_a;
				particleImpl.attachedEmitters_[0].emitter_.reset(particle.position_, null);
			}

			public override void initBurstParticle(Emitter emitter, Particle particle, Particle first)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				ParticleImpl firstImpl = (ParticleImpl)first;
				float dt = 0;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				particleImpl._lifetime = 0F;
				_math.vec3 value_ = _math.vec3_(0F, 0F, 0F);
				particleImpl._Position = _math.addv3_(value_, emitter.position());
				particleImpl._Emitter_rot_ = emitter.rotation();
				particleImpl._Angle = 0F;
				float value_a = 2F;
				float expr_ = (particleImpl._lifetime / value_a);
				float _plot_out;
				float _plot_in0=(expr_<0F?0F:(expr_>1F?1F:expr_));
				_math.PathRes _plot_srch0 = _math.pathRes(0,(_plot_in0-0F)*1F);
				_math.funcLerp(out _plot_out, this._plot[0][_plot_srch0.s],_plot_srch0.i);
				float _path_in = _math.clamp_(_plot_out, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*15F);
				_math.vec2 _path_pos;
				_math.vec2 _path_dir;
				_math.pathLerp2(out _path_pos, out _path_dir, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				_math.vec3 trn3op0 = _math.applyv3quat_(conv3d_, particleImpl._Emitter_rot_);
				_math.addv3(out trn3op0, trn3op0, _math.vec3_(0F,0F,0F));
				_math.vec3 expr_a = _math.addv3_(particleImpl._Position, trn3op0);
				particle.position_ = expr_a;
				particleImpl.attachedEmitters_[0].emitter_.reset(particle.position_, null);
			}

			public override void updateParticle(Emitter emitter, Particle particle, float dt)
			{
				ParticleImpl particleImpl = (ParticleImpl)particle;
				EmitterData emitterData = (EmitterData)emitter.data();

				GeneratorPeriodic generator = (GeneratorPeriodic)emitter.generator();
				GeneratorImpl generatorImpl = (GeneratorImpl)generator.impl();
				bool toTerminate = false;
				particleImpl._lifetime += dt;
				float value_ = 2F;
				float expr_ = (particleImpl._lifetime / value_);
				float _plot_out;
				float _plot_in0=(expr_<0F?0F:(expr_>1F?1F:expr_));
				_math.PathRes _plot_srch0 = _math.pathRes(0,(_plot_in0-0F)*1F);
				_math.funcLerp(out _plot_out, this._plot[0][_plot_srch0.s],_plot_srch0.i);
				float _path_in = _math.clamp_(_plot_out, 0, 1);
				_math.PathRes _path_srch = _math.pathRes(0,(_path_in-0F)*15F);
				_math.vec2 _path_pos;
				_math.vec2 _path_dir;
				_math.pathLerp2(out _path_pos, out _path_dir, this._path[_path_srch.s], _path_srch.i);
				_math.vec3 conv3d_ = _math.vec3_(_path_pos.x, _path_pos.y, 0F);
				_math.vec3 trn3op0 = _math.applyv3quat_(conv3d_, particleImpl._Emitter_rot_);
				_math.addv3(out trn3op0, trn3op0, _math.vec3_(0F,0F,0F));
				_math.vec3 expr_a = _math.addv3_(particleImpl._Position, trn3op0);
				particle.position_ = expr_a;
				particleImpl.attachedEmitters_[0].emitter_.update(dt, particle.position_, null);
				toTerminate |= (particleImpl._lifetime > value_);
				if (toTerminate)
				{
					particle.dead_ = true;
					particleImpl.attachedEmitters_[0].emitter_.disactivate();
				}
			}
			public Emitter_DefaultEmitter()
			{
				generatorCreator_ = (Emitter emitter) => { return new GeneratorPeriodic(emitter, new GeneratorImpl()); };
				name_ = "DefaultEmitter";
				maxNumParticles_ = 100;
				sorting_ = Emitter.Sorting.OldToYoung;
				particleCreator_ = (Effect effect) => {
					ParticleImpl particle = new ParticleImpl();
					particle.attachedEmitters_ = new AttachedEmitter[]
					{
						new AttachedEmitter(new Emitter(effect, effect.Model.emitterModels()[0]), AttachedEmitter.DrawOrder.After, AttachedEmitter.ActivationMode.OnCreate)
					};
					return particle;
				};
				emitterDataCreator_ = () => { return new EmitterData(); };
			}
		}

		public Effect_presim_fps()
		{
			textures_ = new string[] { "test0.png" };
			materials_ = new RenderMaterial[] { RenderMaterial.Normal };
			renderStyles_ = new RenderStyle[] { new RenderStyle(0,new uint[] {0}) };
			frameTime_ = 0.0333333F;
			presimulateTime_ = 2F;
			presimulateFrameTime_ = 0.333333F;
			maxNumRenderCalls_ = 10100;
			maxNumParticles_ = 10000;
			emitterModels_ = new EmitterModel[]{ new Emitter_Children(), new Emitter_DefaultEmitter() };
			activeEmitterModels_ = new uint[] { 1 };
			randomGeneratorCreator_ = () => { return _math.rand_; };
		}
	}
}

#pragma warning restore 219