﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class Turbulence
    {
        private class Test : IBCT
        {
            private bool turbulenceEnabled_;

            public Test(string name, bool turbulenceEnabled) : base(name)
            {
                turbulenceEnabled_ = turbulenceEnabled;
            }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice,
                    generateNoise: turbulenceEnabled_);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.turbulence(), Content);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0));

                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void Enabled()
        {
            using (var game = new Test("Turbulence.Enabled", true))
                game.Run();
        }
    }
}
