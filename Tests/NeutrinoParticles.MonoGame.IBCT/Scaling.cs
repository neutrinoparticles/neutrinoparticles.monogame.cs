﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class Scaling
    {
        private class Test : IBCT
        {
            public Test(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.scaling(), Content, null);

                var effects = new List<NeutrinoParticles.MonoGame.ParticleEffect>();

                effects.Add(new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(100, 100, 0)));
                effects.Add(new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(600, 200, 0),
                    scale: new Vector3(2, 2, 2)));
                effects.Add(new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(300, 400, 0),
                    scale: new Vector3(4, 4, 4)));

                clear();

                foreach (var effect in effects)
                {
                    effect.Update(1);
                    effect.Draw();
                }
                
                check();
            }
        }

        [TestMethod]
        public void Simple()
        {
            using (var game = new Test("Scaling.Simple"))
                game.Run();
        }

        private class TestNonUniform : IBCT
        {
            public TestNonUniform(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.scaling(), Content, null);

                var effects = new List<NeutrinoParticles.MonoGame.ParticleEffect>();

                effects.Add(new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(100, 100, 0)));
                effects.Add(new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(600, 200, 0),
                    scale: new Vector3(2, 1, 2)));
                effects.Add(new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(300, 400, 0),
                    scale: new Vector3(2, 4, 4)));

                clear();

                foreach (var effect in effects)
                {
                    effect.Update(1);
                    effect.Draw();
                }

                check();
            }
        }

        [TestMethod]
        public void NonUniform()
        {
            using (var game = new TestNonUniform("Scaling.NonUniform"))
                game.Run();
        }
    }
}
