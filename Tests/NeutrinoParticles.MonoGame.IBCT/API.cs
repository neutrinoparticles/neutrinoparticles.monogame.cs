﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class API
    {

        private class TestContextNoiseRegeneration : IBCT
        {
            private Vector3? position_;

            public TestContextNoiseRegeneration(string name) : base(name)
            {
            }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice, generateNoise: true);
                neutrinoContext.Shutdown();

                neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice, generateNoise: true);
                neutrinoContext.Shutdown();
            }
        }


        [TestMethod]
        public void ContextNoiseRegeneration()
        {
            using (var game = new TestContextNoiseRegeneration("API.ContextNoiseRegeneration"))
                game.Run();
        }
    }
}
