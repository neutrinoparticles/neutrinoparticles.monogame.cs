﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class Pause
    {
        private class TestConstructor : IBCT
        {
            public TestConstructor(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.pause(), Content, null);

                {
                    var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                        position: new Vector3(400, 300, 0)); // unpaused by default

                    effect.Update(2);
                    clear();
                    effect.Draw();
                    check();
                }

                {
                    var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                        position: new Vector3(400, 300, 0),
                        paused: true);

                    effect.Update(2);
                    clear();
                    effect.Draw();
                    check();
                }
            }
        }

        [TestMethod]
        public void Default()
        {
            using (var game = new TestConstructor("Pause.Constructor"))
                game.Run();
        }

        private class TestRelocateBeforeUnpause : IBCT
        {
            public TestRelocateBeforeUnpause(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.pause(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    paused: true);

                effect.Position = new Vector3(400, 300, 0);
                effect.Paused = false;
                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void RelocateBeforeUnpause()
        {
            using (var game = new TestRelocateBeforeUnpause("Pause.RelocateBeforeUnpause"))
                game.Run();
        }

        private class TestGeneratorsPausedOnStart : IBCT
        {
            public TestGeneratorsPausedOnStart(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.pause(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0),
                    generatorsPaused: true);

                effect.Update(1);
                effect.GeneratorsPaused = false;
                effect.Update(1);
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void GeneratorsPausedOnStart()
        {
            using (var game = new TestGeneratorsPausedOnStart("Pause.GeneratorsPausedOnStart"))
                game.Run();
        }

        private class TestGeneratorsPausedRuntime : IBCT
        {
            public TestGeneratorsPausedRuntime(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.pause(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0));

                effect.Update(1);
                effect.GeneratorsPaused = true;
                effect.Update(1);
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void GeneratorsPausedRuntime()
        {
            using (var game = new TestGeneratorsPausedRuntime("Pause.GeneratorsPausedRuntime"))
                game.Run();
        }
    }
}
