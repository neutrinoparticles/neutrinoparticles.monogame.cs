﻿using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SkiaSharp;
using System;
using SharpDX.MediaFoundation.DirectX;
using SharpDX.Direct3D9;

namespace NeutrinoParticles.MonoGame.IBCT
{
    internal abstract class IBCT : Game
    {
        static readonly string resultsDir = "__results__";
        static readonly string expectDir = "__expect__";
        static readonly string diffDir = "__diff__";

        public class Exception : System.Exception
        {
            private List<string> messages_ = new List<string>();

            public Exception(string msg)
            {
                pushMessage(msg);
            }

            public void pushMessage(string msg)
            {
                messages_.Add(msg);
            }

            public override string ToString()
            {
                string result = "";
                foreach (var msg in messages_)
                {
                    result += msg + "\n";
                }
                return result;
            }
        };

        GraphicsDeviceManager graphics_;
        RenderTarget2D renderTarget_;
        protected int width_, height_;

        private string name_;
        private uint checkIndex_ = 0;

        public Exception error_ = null;

        public IBCT(string name)
        {
            GraphicsAdapter.UseDriverType = GraphicsAdapter.DriverType.FastSoftware;

            graphics_ = new GraphicsDeviceManager(this);

            graphics_.PreferredBackBufferWidth = 800;
            graphics_.PreferredBackBufferHeight = 600;

            graphics_.ApplyChanges();

            Content.RootDirectory = "Content";
            name_ = name;
        }

        public void Run(ref Exception exception)
        {
            RunOneFrame();
            
            if (error_ != null)
            {
                if (exception != null)
                {
                    exception.pushMessage(error_.ToString());
                }
                else
                {
                    exception = error_;
                }
            }
        }

        public new void Run()
        {
            RunOneFrame();

            if (error_ != null)
                throw error_;
        }

        protected override void LoadContent()
        {
            width_ = GraphicsDevice.PresentationParameters.BackBufferWidth;
            height_ = GraphicsDevice.PresentationParameters.BackBufferHeight;

            renderTarget_ = new RenderTarget2D(GraphicsDevice, width_, height_, false, SurfaceFormat.Bgra32, DepthFormat.None);
            GraphicsDevice.SetRenderTarget(renderTarget_);

            LoadContentImpl();

            GraphicsDevice.SetRenderTarget(null);
        }

   /*
        protected void prepare3DMatrices()
        {
            projMatrix_ = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, (float)width_ / (float)height_, 100, 10000);
            viewMatrix_ = Matrix.CreateLookAt(new Vector3(0, 0, -800), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
        }
        */
        protected void clear()
        {
            GraphicsDevice.Clear(new Microsoft.Xna.Framework.Color(128, 128, 128));
        }

        protected void check()
        {
            byte[] data = new byte[width_ * height_ * 4];
            renderTarget_.GetData<byte>(data);

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(width_, height_,
                PixelFormat.Format32bppRgb);

            var bmpData = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height),
                       ImageLockMode.WriteOnly, bmp.PixelFormat);

            Marshal.Copy(data, 0, bmpData.Scan0, data.Length);

            bmp.UnlockBits(bmpData);

            var imageName = name_ + checkIndex_.ToString() + ".png";

            ++checkIndex_;

            SKBitmap actual = convertToSKBitmap(bmp);
            Directory.CreateDirectory(resultsDir);

            saveSKBitmapToFile(actual, Path.Combine(resultsDir, imageName));

            SKBitmap expected;

            try
            {
                expected = SKBitmap.Decode(Path.Combine(expectDir, imageName));
            }
            catch (System.IO.IOException)
            {
                error("No expectation file " + imageName);
                return;
            }

            SKBitmap difference = highlightDifferences(actual, expected, 100);

            if (difference != null)
            {
                Directory.CreateDirectory(diffDir);
                saveSKBitmapToFile(difference, Path.Combine(diffDir, imageName));
                error("Difference found in " + imageName);
                return;
            }
        }

        private static void saveSKBitmapToFile(SKBitmap bmp, string outputPath)
        {
            using (var image = SKImage.FromBitmap(bmp))
            using (var data = image.Encode(SKEncodedImageFormat.Png, 100))
            using (var stream = System.IO.File.OpenWrite(outputPath))
            {
                data.SaveTo(stream);
            }
        }

        private static SKBitmap convertToSKBitmap(System.Drawing.Bitmap bitmap)
        {
            using (var ms = new MemoryStream())
            {
                // Save the System.Drawing.Bitmap to a memory stream
                bitmap.Save(ms, ImageFormat.Bmp);
                ms.Seek(0, SeekOrigin.Begin);

                // Decode the byte array to SKBitmap
                return SKBitmap.Decode(ms);
            }
        }

        public static SKBitmap highlightDifferences(SKBitmap image1, SKBitmap image2, int diffPixelsThreshold)
        {
            var diffImage = new SKBitmap(image1.Width, image1.Height);

            if (image2 == null || image1.Width != image2.Width || image1.Height != image2.Height)
            {
                using (SKCanvas canvas = new SKCanvas(diffImage))
                {
                    // Встановіть колір заливки
                    SKPaint paint = new SKPaint
                    {
                        Style = SKPaintStyle.Fill,
                        Color = SKColors.Red // Змініть колір на бажаний
                    };

                    // Залийте весь бітмап кольором
                    canvas.DrawRect(new SKRect(0, 0, diffImage.Width, diffImage.Height), paint);
                }

                return diffImage;
            }

            for (int y = 0; y < image1.Height; y++)
            {
                for (int x = 0; x < image1.Width; x++)
                {
                    var color1 = image1.GetPixel(x, y);
                    var color2 = image2.GetPixel(x, y);
                    if (color1 != color2)
                    {
                        // Highlight the difference with a red pixel
                        diffImage.SetPixel(x, y, new SKColor(255, 0, 255));
                        --diffPixelsThreshold;
                    }
                    else
                    {
                        diffImage.SetPixel(x, y, color1);
                    }
                }
            }

            if (diffPixelsThreshold > 0)
            {
                diffImage.Dispose();
                return null;
            }

            return diffImage;
        }

        private void error(string msg)
        {
            if (error_ == null)
                error_ = new Exception(msg);
            else
                error_.pushMessage(msg);
        }

        abstract protected void LoadContentImpl();
    }
}
