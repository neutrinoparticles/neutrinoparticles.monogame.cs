﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class Atlas
    {

        private class Test : IBCT
        {
            public Test(string name) : base(name) {}

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var atlas = Content.Load<Texture2D>("atlas");

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.atlas(), Content,
                    atlasesLookup: delegate (string texture)
                    {
                        switch (texture)
                        {
                            case "test0": return new NeutrinoParticles.MonoGame.AtlasTexture(
                                new Vector2(0, 0), new Vector2(0.5f, 1.0f), atlas);
                            case "test1": return new NeutrinoParticles.MonoGame.AtlasTexture(
                                new Vector2(0.5f, 0), new Vector2(0.5f, 1.0f), atlas);
                        }
                        return null;
                    });

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel, 
                    new Vector3(400, 300, 0), null);

                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }


        [TestMethod]
        public void NormalBlending()
        {
            using (var game = new Test("Atlas.Simple"))
                game.Run();
        }
    }
}
