﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class Positioning
    {

        private class TestStatic : IBCT
        {
            private Vector3? position_;

            public TestStatic(string name, Vector3? position) : base(name)
            {
                position_ = position;
            }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.positioning(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel, position: position_);

                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }


        [TestMethod]
        public void StaticDefault()
        {
            using (var game = new TestStatic("Positioning.StaticDefault", null))
                game.Run();
        }

        [TestMethod]
        public void Static400x300()
        {
            using (var game = new TestStatic("Positioning.Static400x300.", new Vector3(400, 300, 0)))
                game.Run();
        }

        private class TestDynamicConsistent : IBCT
        {
            public TestDynamicConsistent(string name) : base(name) {}

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.positioning(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel);

                effect.Position = new Vector3(400, 300, 0);
                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void DynamicConsistent()
        {
            using (var game = new TestDynamicConsistent("Positioning.DynamicConsistent"))
                game.Run();
        }

        private class TestDynamicJump : IBCT
        {
            public TestDynamicJump(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.positioning(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel);

                effect.Position = new Vector3(200, 150, 0);
                effect.Update(1);
                effect.InstantRelocate(new Vector3(400, 300, 0));
                effect.Position = new Vector3(600, 450, 0);
                effect.Update(1);
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void DynamicJump()
        {
            using (var game = new TestDynamicJump("Positioning.DynamicJump"))
                game.Run();
        }
    }
}
