﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class PresimFPS
    {
        private class Test : IBCT
        {
            public Test(string name) : base(name)
            {
            }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.Effect_presim_fps(), Content);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0));

                effect.Update(0);
                clear();
                effect.Draw();
                check();

                effect.Update(1.5F);
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void Default()
        {
            using (var game = new Test("PresimFPS.Default"))
                game.Run();
        }
    }
}
