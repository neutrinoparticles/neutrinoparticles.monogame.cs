﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;


namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class PerModelTexturesBasePath
    {

        private class Test : IBCT
        {
            private Vector3? position_;

            public Test(string name) : base(name)
            {
            }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.pause(), Content,
                    texturesBasePath: "dir1/");

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel, position: new Vector3(400, 300, 0));

                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }


        [TestMethod]
        public void Default()
        {
            using (var game = new Test("PerModelTexturesBasePath.Default"))
                game.Run();
        }
    }
}
