﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class FlexFPS
    {
        private class Test : IBCT
        {
            public Test(string name) : base(name)
            {
            }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.Effect_flex_fps(), Content);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0));

                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void Enabled()
        {
            using (var game = new Test("FlexFPS.Default"))
                game.Run();
        }
    }
}
