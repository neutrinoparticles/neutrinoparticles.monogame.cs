﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class Coloring
    {

        private class TestDefault : IBCT
        {
            public TestDefault(string name) : base(name)
            {
            }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.color(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0));

                effect.Update(4);
                clear();
                effect.Draw();
                check();
            }
        }


        [TestMethod]
        public void Default()
        {
            using (var game = new TestDefault("Coloring.Default"))
                game.Run();
        }
    }
}
