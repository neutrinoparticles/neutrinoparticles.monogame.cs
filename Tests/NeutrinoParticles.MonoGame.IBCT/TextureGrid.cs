﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Reflection;


namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class TextureGrid
    {

        private class TestDefault : IBCT
        {
            public TestDefault(string name) : base(name) {}

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.texture_grid(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel, 
                    position: new Vector3(400, 300, 0));

                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }


        [TestMethod]
        public void Default()
        {
            using (var game = new TestDefault("TextureGrid.Default"))
                game.Run();
        }

        private class TestYAxisUp : IBCT
        {
            public TestYAxisUp(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var width = GraphicsDevice.PresentationParameters.BackBufferWidth;
                var height = GraphicsDevice.PresentationParameters.BackBufferHeight;
                Matrix projMatrix = Matrix.CreateOrthographicOffCenter(0, width, 0, height, -10000, 10000);

                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice,
                    axisYGoesUp: true);
                               

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.texture_grid(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0));

                effect.Update(2);
                clear();
                effect.Draw(projMatrix);
                check();
            }
        }


        [TestMethod]
        public void YAxisUp()
        {
            using (var game = new TestDefault("TextureGrid.YAxisUp"))
                game.Run();
        }

        private class TestOutOfRange : IBCT
        {
            public TestOutOfRange(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.texture_grid_out_of_range(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0));

                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }


        [TestMethod]
        public void OutOfRange()
        {
            using (var game = new TestOutOfRange("TextureGrid.OutOfRange"))
                game.Run();
        }
    }
}
