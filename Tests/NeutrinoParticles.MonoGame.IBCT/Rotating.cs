﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace NeutrinoParticles.MonoGame.IBCT
{
    [TestClass]
    public class Rotating
    {

        private class TestStatic : IBCT
        {
            private Quaternion? rotation_;

            public TestStatic(string name, float? angle) : base(name)
            {
                if (angle.HasValue)
                {
                    rotation_ = Quaternion.CreateFromAxisAngle(new Vector3(0, 0, 1), MathHelper.ToRadians(angle.Value));
                }
            }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.rotating(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0),
                    rotation: rotation_);

                effect.Update(2);
                clear();
                effect.Draw();
                check();
            }
        }


        [TestMethod]
        public void Static()
        {
            IBCT.Exception ex = null;

            using (var game = new TestStatic("Rotating.StaticDefault", null))
                game.Run(ref ex);

            float[] angles = { 45, 90, 135, 180, 225, 270, 315, 360 };

            foreach (float angle in angles)
            {
                using (var game = new TestStatic("Rotating.Static" + angle.ToString() + ".", angle))
                    game.Run(ref ex);

                using (var game = new TestStatic("Rotating.StaticNeg" + angle.ToString() + ".", -angle))
                    game.Run(ref ex);
            }

            if (ex != null)
                throw ex;
        }

        private class TestDynamicJump : IBCT
        {
            public TestDynamicJump(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.rotating(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0));

                var axis = new Vector3(0, 0, 1);
                effect.Rotation = Quaternion.CreateFromAxisAngle(axis, MathHelper.ToRadians(45));
                effect.Update(1);
                effect.InstantRelocate(
                    rotation: Quaternion.CreateFromAxisAngle(axis, MathHelper.ToRadians(90)));
                effect.Rotation = Quaternion.CreateFromAxisAngle(axis, MathHelper.ToRadians(135));
                effect.Update(1);
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void DynamicJump()
        {
            using (var game = new TestDynamicJump("Rotating.DynamicJump"))
                game.Run();
        }

        private class TestSpiral : IBCT
        {
            public TestSpiral(string name) : base(name) { }

            protected override void LoadContentImpl()
            {
                var neutrinoContext = new NeutrinoParticles.MonoGame.Context(GraphicsDevice);

                var effectModel = new NeutrinoParticles.MonoGame.ParticleEffectModel(neutrinoContext,
                    new NeutrinoParticles.rotating(), Content, null);

                var effect = new NeutrinoParticles.MonoGame.ParticleEffect(effectModel,
                    position: new Vector3(400, 300, 0));

                var axis = new Vector3(0, 0, 1);
                effect.Update(0.25f, rotation:
                    Quaternion.CreateFromAxisAngle(axis, MathHelper.ToRadians(90)));
                effect.Update(0.25f, rotation:
                    Quaternion.CreateFromAxisAngle(axis, MathHelper.ToRadians(180)));
                effect.Update(0.25f, rotation:
                    Quaternion.CreateFromAxisAngle(axis, MathHelper.ToRadians(270)));
                effect.Update(0.25f, rotation:
                    Quaternion.CreateFromAxisAngle(axis, MathHelper.ToRadians(360)));
                clear();
                effect.Draw();
                check();
            }
        }

        [TestMethod]
        public void Spiral()
        {
            using (var game = new TestSpiral("Rotating.Spiral"))
                game.Run();
        }
    }
}
